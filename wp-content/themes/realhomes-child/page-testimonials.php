<?php
/*
*   Template Name:  Testimonials
*/
get_header();
?>

<!-- Page Head -->
<?php get_template_part("banners/blog_page_banner"); ?>

<!-- Content -->
<div class="container contents blog-page">
    <div class="row">
        <div class="span9 main-wrap">
            <!-- Main Content -->
            <div class="main">

                <div class="inner-wrapper">
                
                <?php the_field('testimonials_intro'); ?>
                
                    <?php
									 global $post;
									 $myposts = get_posts('numberposts=100&category_name=testimonials');
									 foreach($myposts as $post) :
									   setup_postdata($post);
									 ?>

												<hr / >
												
												<br />
												
												<?php the_content(); ?>
												<h3><?php the_title(); ?></h3>
												
												<div class="clear"></div>
                                                
									 <?php endforeach; ?>
                </div>

            </div><!-- End Main Content -->

        </div> <!-- End span9 -->

        <?php get_sidebar(); ?>

    </div><!-- End contents row -->
</div><!-- End Content -->

<?php get_footer(); ?>