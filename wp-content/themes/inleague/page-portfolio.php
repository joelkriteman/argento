 <?php
/*
 * Template Name: Portfolio
 *
 */
?>

<?php get_header(); ?>
<!-- transitions styles -->
<link rel="stylesheet" href="<?php get_template_directory_uri(); ?>/css/transitions.css" type="text/css" media="screen" />

		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php the_title(); ?>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->

		<div id="portfolio" class="container_12">
			<section class="grid_12">
				<ul id="filter" data-option-key="filter">
					<li class="current"><a href="#" data-option-value="*">All</a></li>
					<?php
						// Get the taxonomy
						$terms = get_terms('filter', $args);
						
						// set a count to the amount of categories in our taxonomy
						$count = count($terms); 
						
						// set a count value to 0
						$i=0;
						
						// test if the count has any categories
						if ($count > 0) {
							
							// break each of the categories into individual elements
							foreach ($terms as $term) {
								
								// increase the count by 1
								$i++;
								
								// rewrite the output for each category
								$term_list .= '<li><a href="#" data-option-value=".'. $term->slug .'">' . $term->name . '</a></li>';
								
								// if count is equal to i then output blank
								if ($count != $i)
								{
									$term_list .= '';
								}
								else 
								{
									$term_list .= '';
								}
							}
							
							// print out each of the categories in our new format
							echo $term_list;
						}
					?>
				</ul><!-- end #filter -->
				
				<div id="thumbnails">
				
				<?php 
		
					$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 6 );  
					$loop = new WP_Query( $args );  
						while ( $loop->have_posts() ) : $loop->the_post();
    
				?>
				
				<?php $terms = get_the_terms( get_the_ID(), 'filter' ); ?>

					<div class="element <?php foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; } ?> <?php echo $count; ?>">
						<div class="imageWraper">
							<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
								<?php the_post_thumbnail( 'portfolio-thumb' ); ?>
								<span></span>
							</a>
						</div><!-- end .imageWrap -->
						<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
						<span>
							<?php
								foreach ($terms as $term) {
									echo strtolower(preg_replace('/\s+/', '-', $term->name)). '/ ';
								}
							?>
						</span>
					</div>
					
					<?php $count++; ?>
					
				<?php endwhile; ?>
				
				</div><!-- end #thumbnails -->
			</section>
		</div><!-- end #portfolioWraper -->

<?php get_footer(); ?>