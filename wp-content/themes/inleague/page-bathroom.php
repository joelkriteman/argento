 <?php
/*
 * Template Name: Bathroom Sales
 */
?>

<?php get_header() ?>


		<div class="margintwenty">
		  <img src="<?php the_field('header_image'); ?>" alt="" />
		</div>

<div class="clear"></div>

  		<div class="margintwenty">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
      </div>

<div class="clear"></div>
<hr />

<div class="one_third">
  <h4><?php the_field('left_title'); ?></h4>
  <?php the_field('left_text'); ?>
</div>

<div class="one_third">
  <h4><?php the_field('centre_title'); ?></h4>
  <?php the_field('centre_text'); ?>
</div>

<div class="one_third last">
  <h4><?php the_field('right_title'); ?></h4>
  <?php the_field('right_text'); ?>
</div>

<div class="clear"></div>

<div class="margintwenty">
  <h4><?php the_field('bottom_title'); ?></h4>
  <?php the_field('bottom_text'); ?>
</div>

<div id="servicesthumbs" class="bathroomsales">

<?php if( get_field('ppimage1') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage1'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage1'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage2') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage2'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage2'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage3') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage3'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage3'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   <?php if( get_field('ppimage4') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage4'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage4'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage5') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage5'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage5'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage6') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage6'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage6'); ?>" /></a>
         </div>
	   <?php endif; ?>


<?php if( get_field('ppimage7') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage7'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage7'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage8') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage8'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage8'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage9') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage9'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage9'); ?>" /></a>
         </div>
	   <?php endif; ?>


<?php if( get_field('ppimage10') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage10'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage10'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage11') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage11'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage11'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage12') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage12'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage12'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   <?php if( get_field('ppimage13') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage13'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage13'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   <?php if( get_field('ppimage14') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage14'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage14'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage15') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage15'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage15'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage16') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage16'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage16'); ?>" /></a>
         </div>
	   <?php endif; ?>


<?php if( get_field('ppimage17') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage17'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage17'); ?>" /></a>
         </div>
	   <?php endif; ?>

     <?php if( get_field('ppimage18') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage18'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage18'); ?>" /></a>
         </div>
	   <?php endif; ?>
	   
	   
	   <?php if( get_field('ppimage19') ): ?>
	       <div class="post-thumbnail small-thm">
              <a href="<?php the_field('ppimage19'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage19'); ?>" /></a>
         </div>
	   <?php endif; ?>


<?php if( get_field('ppimage20') ): ?>
	       <div class="post-thumbnail small-thm">
            <a href="<?php the_field('ppimage20'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage20'); ?>" /></a>
         </div>
	   <?php endif; ?>

</div>

<?php get_footer() ?>