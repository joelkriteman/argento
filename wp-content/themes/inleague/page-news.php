 <?php
/*
 * Template Name: News
 *
 */
?>

<?php get_header() ?>

	
	<div class="clear"></div>
	
	<div id="pageintro">
	   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
	</div>
	
	<hr>
	

	
<div id="newspage">
<?php
    $args = array( 'category' => 5, 'post_type' =>  'post', 'numberposts' =>100 ); 
    $postslist = get_posts( $args );    
    foreach ($postslist as $post) :  setup_postdata($post); 
    ?>
    <div class="blogPost">
      <div class="post-thumbnail small-thm">
        <?php the_post_thumbnail(); ?>
      </div>
      <div class="title"><?php the_title(); ?></div>
                
                <div class="post-content">
	                  <?php the_excerpt(); ?>
	                  <a href="<?php the_permalink(); ?>" class="readmore">read more</a>
	              </div>
      </div>
       
   
    <?php endforeach; ?>
</div>

<?php get_footer() ?>