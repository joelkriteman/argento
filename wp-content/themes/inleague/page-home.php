 <?php
/*
 * Template Name: Front Page
 *
 */
?>

<?php get_header() ?>


		<?php
		
		
		// Slider
		if ( of_get_option('home_show_slider') ) { include("home_slider.php"); }
		
		// Slogan
		if ( of_get_option('home_show_slogan') ) { include("home_slogan.php"); }
		
		// Services
		if ( of_get_option('home_show_services') ) { include("home_services.php"); }
		
		// Recent Work
		if ( of_get_option('home_show_recent_work') ) { include("home_recent.php"); }
		
		// About
		if ( of_get_option('home_show_about') ) { include("home_about.php"); }
		
		?>

<script>
// ----- Nivo Slider ----- //

jQuery(window).load(function() {
    jQuery('#slider').nivoSlider({
        effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
        slices: 15, // For slice animations
        boxCols: 8, // For box animations
        boxRows: 4, // For box animations
        animSpeed: 500, // Slide transition speed
        pauseTime: 3000, // How long each slide will show *3000
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: <?php if ( of_get_option('home_show_slider_arrows') ) { echo 'true'; } else { echo 'false'; } ?>, // Next & Prev navigation
        directionNavHide: true, // Only show on hover
        controlNav: <?php if ( of_get_option('home_show_slider_control') ) { echo 'true'; } else { echo 'false'; } ?>,// 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        controlNavThumbsFromRel: false, // Use image rel for thumbs
        controlNavThumbsSearch: '.jpg', // Replace this with...
        controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
        keyboardNav: true, // Use left & right arrows
        pauseOnHover: true, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        captionOpacity: 0, // Universal caption opacity
        prevText: 'Prev', // Prev directionNav text
        nextText: 'Next', // Next directionNav text
        randomStart: false, // Start on a random slide
        beforeChange: function(){}, // Triggers before a slide transition
        afterChange: function(){}, // Triggers after a slide transition
        slideshowEnd: function(){}, // Triggers after all slides have been shown
        lastSlide: function(){}, // Triggers when last slide is shown
        afterLoad: function(){} // Triggers when slider has loaded
    });
});
</script>

<?php get_footer() ?>