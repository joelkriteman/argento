<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php the_title(); ?>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="page" class="container_12">
			<section id="content" role="main" class="grid_8">
			
				<div class="blogPost">
    
                    <article class="post">
                    
                    	<div class="post-content">
                        	
                        	<?php the_content(); ?>
                			<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'inLEAGUE' ), 'after' => '' ) ); ?>
                			<?php edit_post_link( __( 'Edit', 'inLEAGUE' ), '', '' ); ?>
                    
                    	</div><!-- end .post-content -->
                    
                    </article><!-- end .post -->
                
                </div><!-- end .blogPost -->
                
                <?php endwhile; ?>

				<?php comments_template( '', true ); ?>

			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>