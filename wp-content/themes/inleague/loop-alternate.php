<?php while ( have_posts() ) : the_post(); ?>
			    
<div id="post-<?php the_ID(); ?>" <?php post_class('blogPost'); ?>>
    
    <article class="post">
    
    	<div class="post-meta grid_3 alpha">
    	
    		<div class="post-thumbnail small-thm">
            
            	<?php echo '<a href="', get_permalink(), '">';
                    if (has_post_thumbnail()) {
                        the_post_thumbnail( 'small-thumb' );
                    }
                    else {
                        echo
                            '<img src="',
                            get_template_directory_uri(), '/images/griver-thm.jpg',
                            '" alt="thumbnail" />';
                    }
                    echo '</a>';
                ?>
            
            </div><!-- end .post-thumbnail -->
    
    	</div><!-- end .post-meta -->
    
    	<div class="post-content grid_5 omega">
    		
    		<h1 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
    
    		<span class="by-meta">by <?php the_author_posts_link(); ?> | <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>
<?php edit_post_link( __( 'Edit', 'inLEAGUE' ), '| ', '' ); ?></span>
        	
        	<?php // Custom Excerpt length, use the_excerpt() for default ?>
    		<?php in_custom_excerpt(35); ?>
    
    	</div><!-- end .post-content -->
    
    </article><!-- end .post -->

</div><!-- end .blogPost -->

<?php endwhile; // End the loop. Whew. ?>