<?php get_header(); ?>

		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">
			
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    
			    <div id="post">

                    <div class="post-thumbnail small-thm">
        <a href="<?php the_field('ppimage1'); ?>" rel="lightbox[<?php the_title(); ?>]"><?php the_post_thumbnail(); ?></a>
        
        

          <?php 
										  if( get_field('ppimage2') ):
	?>
	   <a href="<?php the_field('ppimage2'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
<?php
endif;
 
?>

          <?php 
										  if( get_field('ppimage3') ):
	?>
	   <a href="<?php the_field('ppimage3'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
<?php
endif;
 
?>

          <?php 
										  if( get_field('ppimage4') ):
	?>
	   <a href="<?php the_field('ppimage4'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	<?php
endif;
 
?>


          <?php 
										  if( get_field('ppimage5') ):
	?>
	   <a href="<?php the_field('ppimage5'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>


          <?php 
										  if( get_field('ppimage6') ):
	?>
	   <a href="<?php the_field('ppimage6'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>


          <?php 
										  if( get_field('ppimage7') ):
	?>
	   <a href="<?php the_field('ppimage7'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>


          <?php 
										  if( get_field('ppimage8') ):
	?>
	   <a href="<?php the_field('ppimage8'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

          <?php 
										  if( get_field('ppimage9') ):
	?>
	   <a href="<?php the_field('ppimage9'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

          <?php 
										  if( get_field('ppimage10') ):
	?>
	   <a href="<?php the_field('ppimage10'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage11') ):
	?>
	   <a href="<?php the_field('ppimage11'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>


<?php 
										  if( get_field('ppimage12') ):
	?>
	   <a href="<?php the_field('ppimage12'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage13') ):
	?>
	   <a href="<?php the_field('ppimage13'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>


<?php 
										  if( get_field('ppimage14') ):
	?>
	   <a href="<?php the_field('ppimage14'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage15') ):
	?>
	   <a href="<?php the_field('ppimage15'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage16') ):
	?>
	   <a href="<?php the_field('ppimage16'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage17') ):
	?><li>
	   <a href="<?php the_field('ppimage17'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 </li><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage18') ):
	?>
	   <a href="<?php the_field('ppimage18'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage19') ):
	?>
	   <a href="<?php the_field('ppimage19'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage20') ):
	?><li>
	   <a href="<?php the_field('ppimage20'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 </li><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage21') ):
	?>
	   <a href="<?php the_field('ppimage21'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>

<?php 
										  if( get_field('ppimage22') ):
	?>
	   <a href="<?php the_field('ppimage22'); ?>" rel="lightbox[<?php the_title(); ?>]"></a>
	 <?php
endif;
 
?>



                    
                    <article class="post">
                    
                    	<div class="post-meta">
                    	
      
                 		<div class="availabletop">
        <div class="title"><?php the_title(); ?></div>
        <?php the_field('address'); ?><br />
       <div class="price">Price: <?php the_field('price'); ?></div>
       <div class="price">Yield: <?php the_field('yield'); ?></div>
       Instant Equity: <?php the_field('instant_equity'); ?><br />
      </div>

	
                    
                    	</div><!-- end .post-meta -->
                    
                    	<div class="post-content">
	                    	
	                    	<?php the_content(); ?>
	                    	<a class="enquiry" href="mailto:info@argentoproperty.com?subject=I'm%20interested%20in%20<?php the_title(); ?>&body=<?php the_permalink(); ?>">SUBMIT ENQUIRY</a><div class="clear"></div><br />
	                    	<div class="tel">T</div>0131 242 0009<br />
                        <div class="tel">M</div>07967 393 635
	                    	
                    
                    	</div><!-- end .post-content -->
                    
                    </article><!-- end .post -->
                
                </div><!-- end .blogPost -->

                <?php endwhile; endif; ?>			    
			    
			    
			    				
			</section><!-- end #content -->


<?php get_footer(); ?>