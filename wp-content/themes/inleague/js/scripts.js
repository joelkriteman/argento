var $j = jQuery.noConflict();

$j(document).ready(function() {


// ----- External links ----- //

	
	$j('a[rel=external]').attr('target', 'blank');
	

// ----- Dropdown ----- //
	
	$j('header nav#main li').hover(function() {
		$j(this).find('ul:first').stop().fadeIn('fast');
	},function(){
		$j(this).find('ul:first').stop().fadeOut('fast');
	});


// ----- Recent Work/Portfolio li padding fix ----- //

	$j('#recentWork ul li:nth-child(3n)').addClass('last');
	$j('#thumbnails div:nth-child(3n)').addClass('last');


// ----- Recent Work/Portfolio Fade Effect ----- //

	$j('#recentWork .imageWraper a').hover(function() {
		$j(this).find('span').fadeTo('500', 0.6);
    },function() {
    	$j(this).find('span').fadeTo('fast', 0);
    });
    
    $j('#portfolio .imageWraper a').hover(function() {
		$j(this).find('span').fadeTo('500', 0.5);
    },function() {
    	$j(this).find('span').fadeTo('fast', 0);
    });
    
    
	// ===== Testimonials ===== //
	
	// Hiding all the testimonials, except for the first one.
	$j('#testimonials li').hide().eq(0).show();
	
	// A self executing named function that loops through the testimonials:
	(function showNextTestimonial(){
	
		// Wait for 7.5 seconds and hide the currently visible testimonial:
		$j('#testimonials li:visible').delay(5000).fadeOut('slow',function(){
	
			// Move it to the back:
			$j(this).appendTo('ul#testimonials');
	
			// Show the next testimonial:
			$j('#testimonials li:first').fadeIn('slow',function(){
	
				// Call the function again:
				showNextTestimonial();
			});
		});
	})();


// ----- Tabs ----- //
	
	var panelSelector = $j('.tabs li a');
	var panel = $j('div.panel');
	
	panel.hide().filter(':first').show();
	
	panelSelector.click(function(e) {
	
		panel.hide();
		panel.filter(this.hash).fadeIn(1000);
		
		panelSelector.parent().removeClass('active');
		
		$j(this).parent().addClass('active');
		
		e.preventDefault();
	});



// ----- Case Study Image Switcher ----- //

	//quick array to retrieve the value of the href attrivutes
	var attrArray = [];
	
	$j('a.thmSelector').each(function() {
		
		var attr = $j(this).attr('href');
		attrArray.push(attr);
		
	});
	
	
	//set first value in the array as default preview image
	if ($j('#largeImage img').length == 0) {
	
		$j('<img src="' + attrArray[0] +'" alt="Large Preview" />').appendTo('#largeImage');
	
	}

	$j('a.thmSelector').click(function(e) {
	
		var imgSrc = $j(this).attr('href');
		
		
		//check if there is an image
		if ($j('#largeImage img').length != 0) {
			
			//remove image
			$j('#largeImage img').remove();
			
			//new image to append
			var newImg = $j('<img src="' + imgSrc +'" alt="Large Preview" />').hide().fadeIn(1500);
			
			//add new image from imgSrc value
			$j('#largeImage').append(newImg);
			
		}
	
		e.preventDefault();
	});



// ----- Scroll to top ----- //

	var backToTop = $j('<a class="button scrollTop" href="#">Top</a>');
	
	backToTop.appendTo('body').hide();

	// show/hide back-to-top button
	$j(window).scroll(function() {
	
		if ($j(window).scrollTop() >= 230) {
		
			backToTop.fadeIn('slow');
			
		} else {
		
			backToTop.fadeOut('fast');
			
		}
	
	});
   
    backToTop .click(function(){
        $j('html, body').animate({scrollTop:0}, 'slow');
        return false;
    });

});