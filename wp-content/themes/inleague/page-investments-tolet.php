 <?php
/*
 * Template Name: Investments - To Let
 *
 */
?>

<?php get_header() ?>

	
	<div class="clear"></div>
	

<?php
    $args = array( 'category' => 29, 'post_type' =>  'post', 'numberposts' =>100 ); 
    $postslist = get_posts( $args );    
    foreach ($postslist as $post) :  setup_postdata($post); 
    ?>
    <div class="blogPost">
      <div class="post-thumbnail small-thm">
        <?php the_post_thumbnail(); ?>
      </div>
      
      <div class="availabletop">
        <div class="title"><?php the_title(); ?></div>
        <?php the_field('address'); ?><br />
       <div class="price">Price: <?php the_field('price'); ?></div>
       <div class="price">Yield: <?php the_field('yield'); ?></div>
       Instant Equity: <?php the_field('instant_equity'); ?><br />
      </div>

      
      <div class="excerpt" id="availablecontent"><?php the_excerpt(); ?><br />
        <a href="<?php the_permalink(); ?>" class="readmore">read more</a>
      </div>
      
      </div>
       
   
    <?php endforeach; ?>


<?php get_footer() ?>