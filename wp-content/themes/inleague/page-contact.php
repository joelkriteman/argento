 <?php
/*
 * Template Name: Contact
 *
 */
?>

<?php get_header() ?>


<div class="clear"></div>

<div class="one_third">

  <strong>Registered Office:</strong><br />
  1 St Colme Street<br />
  Edinburgh<br />
  EH3 5DG<br /><br />

  <div class="tel">T</div>0131 242 0009<br />
  <div class="tel">M</div>07967 393 635<br /><br />
  <a href="info@argentoproperty.com">info@argentoproperty.com</a>
  
  <div class="socialicons">
    <a href="https://www.facebook.com/pages/Argento-Property/267734283288799" target="_blank" class="fb">f</a><a href="http://uk.linkedin.com/in/silverstoric" target="_blank" class="fb">i</a><a href="https://twitter.com/argentoproperty" target="_blank">l</a>
  </div>

</div>





<div class="clear"></div>
<hr />

  <div class="one_half">
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
  </div>
  
  <div class="one_half last">
   
  </div>
  
  <p id="registered">Registered in Scotland No. SC420728</p>
  <div id="copyright">
    Site content &copy; 2012 Argento Property Limited
  </div>
  <div id="credit">
    Social Media Type by <a href="http://www.fontfabric.com" target="_blank">Fontfabric</a>
  </div>


<?php get_footer() ?>