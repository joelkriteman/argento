<?php get_header(); ?>

		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">
			
			
			
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    
			    <div id="post">

                    <div class="post-thumbnail small-thm">
        <?php the_post_thumbnail(); ?>
      </div>
      
            <div class="title"><?php the_title(); ?></div>
            
                
                <?php if(in_array('yes', get_field('email_opt_in') )): ?><div id="optin">
    <?php echo do_shortcode( '[contact-form-7 id="1171" title="opt in"]' ); ?></div>
<?php endif; ?>
                
                <div class="post-content">
	                  <?php the_content(); ?>
	                  
	              </div>
	
                    
                    	</div><!-- end .post-meta -->
                    
                    	
                    
                    </article><!-- end .post -->
                
                </div><!-- end .blogPost -->

                <?php endwhile; endif; ?>			    
			    
			    
			    				
			</section><!-- end #content -->


<?php get_footer(); ?>