 <?php
/*
 * Template Name: Archives
 *
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php the_title(); ?>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="page" class="container_12">
			<section id="content" role="main" class="grid_8">

				<?php the_content(); ?>
				<?php edit_post_link( __( 'Edit', 'inLEAGUE' ), '', '' ); ?>
				
				<div class="pageDivider"></div>
								
				<div class="grid_4 alpha">
					<h2>Last 20 Posts</h2>
					<ul>
						<?php wp_get_archives('type=postbypost&limit=20'); ?>
					</ul>
				</div>
				
				<div class="grid_4 omega">				
					<h2>Archives by Month</h2>
					<ul>
						<?php wp_get_archives('show_post_count=1'); ?>
					</ul>
				</div>
				
				<div class="clear"></div>
				
				<h2>Archives by Subject</h2>
				<ul>
					<?php wp_list_categories('title_li='); ?>
				</ul>
				
				
			</section><!-- end #content -->

<?php endwhile; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>