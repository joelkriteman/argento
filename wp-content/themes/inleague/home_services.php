<div class="one_half last">

<div id="homeServices">

	<?php
	
		$page = of_get_option('home_services_page');
		
		$post_id = get_post($page);
		$content = $post_id->post_content;
		$content = apply_filters('the_content', $content);
		$content = str_replace(']]>', ']]>', $content);
		
		echo $content;
	
	?>

</div><!-- end #homeServices -->

</div>