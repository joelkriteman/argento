	<aside id="sidebar" role="sidebar" class="grid_4">
		<ul>
<?php
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	
			<li>
				<?php get_search_form(); ?>
			</li>

			<li>
				<h3 class="heading"><span><?php _e( 'Archives', 'inLEAGUE' ); ?></span></h3>
				<ul class="arrow">
					<?php wp_get_archives( 'type=monthly' ); ?>
				</ul>
			</li>

			<li>
				<h3 class="heading"><span><?php _e( 'Meta', 'inLEAGUE' ); ?></span></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</li>

		<?php endif; // end primary widget area ?>
			</ul>

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

			<ul>
				<?php dynamic_sidebar( 'secondary-widget-area' ); ?>
			</ul>

<?php endif; ?>

	</aside><!-- #sidebar -->
</div><!-- end #blog -->