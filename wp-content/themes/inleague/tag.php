<?php get_header(); ?>

	<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php printf( __( 'Tag Archives: ', 'inLEAGUE') ); ?>
					<span><?php
						printf( __( '%s', 'inLEAGUE' ), '' . single_tag_title( '', false ) . '' );
					?>
					</span>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">

				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) ) {
						echo '' . $category_description . '';
						echo '<div class="divider"></div><br /><br />';
					} else {
						// do nothing
					}

						get_template_part( 'loop', 'alternate' );
				?>

			    <div id="pagination">
			    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<div id="prev"><?php next_posts_link( __( '&larr; Previous', 'inLEAGUE' ) ); ?></div>
					<div id="prev"><?php previous_posts_link( __( 'Next posts &rarr;', 'inLEAGUE' ) ); ?></div>
				<?php endif; ?>
				</div><!-- end #pagination -->
				
			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
