<?php if ( post_password_required() ) : ?>
				<p><?php _e( 'This post is password protected. Enter the password to view any comments.', 'inLEAGUE' ); ?></p>
<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

	<div id="allComments">

<?php if ( have_comments() ) : ?>

	    	<div id="commentsHeader">
	    		<h2>
		    		<?php
			    		printf( __( 'Comments', 'inLEAGUE' ),
			number_format_i18n( get_comments_number() ), '' . get_the_title() . '' );
					?>
	    		</h2>
	    		<div id="addComment">
	    			<a href="#respond" class="button">Add Your Comment</a>
	    		</div><!-- end #addComment -->
	    	</div><!-- end #commentsHeader -->

			<ul id="comments">
				<?php
					/* Loop through and list the comments. Tell wp_list_comments()
					 * to use inLEAGUE_comment() to format the comments.
					 * If you want to overload this in a child theme then you can
					 * define inLEAGUE_comment() and that will be used instead.
					 * See inLEAGUE_comment() in inLEAGUE/functions.php for more.
					 */
					wp_list_comments( array( 'callback' => 'inLEAGUE_comment' ) );
				?>
			</ul>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
				<?php previous_comments_link( __( '&larr; Older Comments', 'inLEAGUE' ) ); ?>
				<?php next_comments_link( __( 'Newer Comments &rarr;', 'inLEAGUE' ) ); ?>
<?php endif; // check for comment navigation ?>

<?php else : // or, if we don't have comments:

	/* If there are no comments and comments are closed,
	 * let's leave a little note, shall we?
	 */
	if ( ! comments_open() ) :
?>
	<p><?php _e( 'Comments are closed.', 'inLEAGUE' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>

	
	<?php
		
		$comments_args = array(
        // remove "Text or HTML to be displayed after the set of comment fields"
        'comment_notes_after' => '',
);
	 comment_form($comments_args); ?>
	        
    </div><!-- end #allComments -->