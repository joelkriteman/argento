<?php

// ===== Create the Custom Post Type ===== // 

add_action('init', 'testimonials_register');  
  
 
function testimonials_register() {  
    
    //Arguments to create post type.
    $args = array(
    	'label' => 'Testimonials',
    	'public' => true,
    	'show_ui' => true,
    	'menu_icon' => get_template_directory_uri() . '/images/icon-testimonials.png',
    	'show_in_menu' => true,
    	'capability_type' => 'post',
    	'hierarchical' => true,
    	'rewrite' => array('slug' => ''),
    	'menu_position' => 4,
    	'supports' => array('title','editor'),
    	'labels' => array (
			  'name' => 'Testimonials',
			  'singular_name' => 'Testimonial',
			  'menu_name' => 'Testimonials',
			  'add_new' => 'Add New Testimonial',
			  'add_new_item' => 'Add New Testimonial',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit',
			  'new_item' => 'New Testimonial',
			  'view' => 'View Testimonial',
			  'view_item' => 'View Testimonial',
			  'search_items' => 'Search Testimonials',
			  'not_found' => 'No Testimonials Found',
			  'not_found_in_trash' => 'No Testimonials Found in Trash',
			),
		);  
  
  	//Register type and custom taxonomy for type.
    register_post_type( 'testimonials' , $args );
}

// ===== Testimonials messages ===== //

function testimonials_messages($messages) {  
    $messages[__( 'testimonials', 'inLEAGUE' )] =  
        array(  
            0 => '',  
            1 => sprintf(('Testimonial Item Updated. <a href="%s">View Testimonial</a>'), esc_url(get_permalink($post_ID))),  
            2 => __('Custom Field Updated.', 'inLEAGUE'),  
            3 => __('Custom Field Deleted.', 'inLEAGUE'),  
            4 => __('Testimonial Updated.', 'inLEAGUE'),  
            5 => isset($_GET['revision']) ? sprintf( __('Testimonial Restored To Revision From %s', 'inLEAGUE'), wp_post_revision_title((int)$_GET['revision'], false)) : false,  
            6 => sprintf(__('Testimonial Published. <a href="%s">View Testimonial</a>', 'inLEAGUE'), esc_url(get_permalink($post_ID))),  
            7 => __('Testimonial Saved.', 'inLEAGUE'),  
            8 => sprintf(__('Testimonial Submitted. <a target="_blank" href="%s">Preview Testimonial</a>', 'inLEAGUE'), esc_url( add_query_arg('preview', 'true', get_permalink($post_ID)))),  
            9 => sprintf(__('Testimonial Scheduled For: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testimonial</a>', 'inLEAGUE'), date_i18n( __( 'M j, Y @ G:i', 'inLEAGUE' ), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),  
            10 => sprintf(__('Testimonial Draft Updated. <a target="_blank" href="%s">Preview Testimonial</a>', 'inLEAGUE'), esc_url( add_query_arg('preview', 'true', get_permalink($post_ID)))),  
        );  
    return $messages;  
  
}
add_filter( 'post_updated_messages', 'testimonials_messages' ); 



// ===== Visit Site Link ===== //

// creates the meta box
function add_meta_testimonials() {
	add_meta_box('testimonials-meta', __('Testimonial Info', 'inLEAGUE'),
	'testimonial_meta_options', 'testimonials',
	'normal', 'high');
}

add_action('admin_init', 'add_meta_testimonials');


// Build the metta box content
function testimonial_meta_options() {
	global $post;
	if ( define('DOING_AUTOSAVE', '') && DOING_AUTOSAVE )
		return $post_id;
	
	$custom = get_post_custom($post->ID);
	$company = $custom['company'][0];
	$website = $custom['website'][0];

	?>
	
	<div class="testimonial-extras">
	
		<?php
			$website = ($website == '') ? 'http://' : $website;
		?>
		<div>
			<label>Company: </label><input name="company" value="<?php echo $company ?>" />
		</div>
		<div>
			<label>Website: </label><input name="website" value="<?php echo $website ?>" />
		</div>	
	</div>
	
	<style>
		.testimonial-extras div {margin-bottom: 10px;}
		.testimonial-extras label {width: 100px; float: left;line-height: 1.4em;}
	</style>

<?php
}

// save portfolio meta data
add_action('save_post', 'testimonial_save_meta');

function testimonial_save_meta() {
	global $post;
	
	if ( define('DOING_AUTOSAVE', '') && DOING_AUTOSAVE ) {
		// if you remove this the sky will fall on your head.
		return $post_id;
	} else {
		update_post_meta($post->ID, 'name', $_POST['name']);
		update_post_meta($post->ID, 'company', $_POST['company']);
		update_post_meta($post->ID, 'website', $_POST['website']);
	}
}