<?php

// ===== Create the Custom Post Type ===== // 

add_action('init', 'portfolio_register');  
  
 
function portfolio_register() {  
    
    //Arguments to create post type.
    $args = array(
    	'label' => 'Portfolio',
    	'public' => true,
    	'show_ui' => true,
    	'menu_icon' => get_template_directory_uri() . '/images/icon-portfolio.png',
    	'show_in_menu' => true,
    	'capability_type' => 'post',
    	'hierarchical' => true,
    	'rewrite' => array('slug' => ''),
    	'menu_position' => 4,
    	'supports' => array('title','editor','revisions','thumbnail',),
    	'labels' => array (
			  'name' => 'Portfolio',
			  'singular_name' => 'Portfolio Item',
			  'menu_name' => 'Portfolio',
			  'add_new' => 'Add New Item',
			  'add_new_item' => 'Add New Item',
			  'edit' => 'Edit',
			  'edit_item' => 'Edit Portfolio Item',
			  'new_item' => 'New Portfolio Item',
			  'view' => 'View Portfolio Item',
			  'view_item' => 'View Portfolio Item',
			  'search_items' => 'Search Portfolio',
			  'not_found' => 'No Portfolio Found',
			  'not_found_in_trash' => 'No Portfolio Found in Trash',
			),
		);  
  
  	//Register type and custom taxonomy for type.
    register_post_type( 'portfolio' , $args );
}

// ===== Portfolio messages ===== //

function portfolio_messages($messages) {  
    $messages[__( 'portfolio', 'inLEAGUE' )] =  
        array(  
            0 => '',  
            1 => sprintf(('Portfolio Item Updated. <a href="%s">View portfolio</a>'), esc_url(get_permalink($post_ID))),  
            2 => __('Custom Field Updated.', 'inLEAGUE'),  
            3 => __('Custom Field Deleted.', 'inLEAGUE'),  
            4 => __('Portfolio Item Updated.', 'inLEAGUE'),  
            5 => isset($_GET['revision']) ? sprintf( __('Portfolio Item Restored To Revision From %s', 'inLEAGUE'), wp_post_revision_title((int)$_GET['revision'], false)) : false,  
            6 => sprintf(__('Portfolio Item Published. <a href="%s">View Portfolio</a>', 'inLEAGUE'), esc_url(get_permalink($post_ID))),  
            7 => __('Portfolio Item Saved.', 'inLEAGUE'),  
            8 => sprintf(__('Portfolio Item Submitted. <a target="_blank" href="%s">Preview Portfolio</a>', 'inLEAGUE'), esc_url( add_query_arg('preview', 'true', get_permalink($post_ID)))),  
            9 => sprintf(__('Portfolio Item Scheduled For: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Portfolio Item</a>', 'inLEAGUE'), date_i18n( __( 'M j, Y @ G:i', 'inLEAGUE' ), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),  
            10 => sprintf(__('Portfolio Item Draft Updated. <a target="_blank" href="%s">Preview Portfolio</a>', 'inLEAGUE'), esc_url( add_query_arg('preview', 'true', get_permalink($post_ID)))),  
        );  
    return $messages;  
  
}

add_filter( 'post_updated_messages', 'portfolio_messages' ); 


// ===== Porfolio Filter ===== //

function portfolio_filter() {  
    register_taxonomy(  
        __( 'filter', 'inLEAGUE' ),  
        array(__( 'portfolio', 'inLEAGUE' )),  
        array(  
            "hierarchical" => true,  
            "label" => __( 'Filter', 'inLEAGUE' ),  
            "singular_label" => __( 'Filter', 'inLEAGUE' ),  
            "rewrite" => array(  
                'slug' => 'filter',  
                'hierarchical' => true  
            )  
        )  
    );  
}

add_action( 'init', 'portfolio_filter', 0 );

// ===== Visit Site Link ===== //

// creates the meta box
function add_meta_site_link() {
	add_meta_box('portfolio-meta', __('Project Link', 'inLEAGUE'),
	'portfolio_meta_options', 'portfolio',
	'normal', 'high');
}

add_action('admin_init', 'add_meta_site_link');


// Build the metta box content
function portfolio_meta_options() {
	global $post;
	if ( define('DOING_AUTOSAVE', '') && DOING_AUTOSAVE )
		return $post_id;
	
	$custom = get_post_custom($post->ID);
	$website = $custom['website'][0];

	?>
	
	<div class="portfolio-extras">
	
		<?php
			$website = ($website == '') ? 'http://' : $website;
		?>
	
		<div>
			<label>Project Link: </label><input name="website" value="<?php echo $website ?>" />
		</div>	
	</div>
	
	<style>
		.portfolio-extras label {width: 100px; float: left;line-height: 1.4em;}
	</style>

<?php
}

// save portfolio meta data
add_action('save_post', 'portfolio_save_meta');

function portfolio_save_meta() {
	global $post;
	
	if ( define('DOING_AUTOSAVE', '') && DOING_AUTOSAVE ) {
		// if you remove this the sky will fall on your head.
		return $post_id;
	} else {
		update_post_meta($post->ID, 'website', $_POST['website']);
	}
}