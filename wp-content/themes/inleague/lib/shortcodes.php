<?php

// ===== Replace WP autop formatting  ===== //

if ( ! function_exists( 'am_remove_wpautop' ) ) {
	function am_remove_wpautop( $content ) {
		$content = do_shortcode( shortcode_unautop( $content ) );
		$content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
		return $content;
	}
}

// ----- Divider ----- //

function am_divider() {
	return '<div class="pageDivider"></div>';
}

add_shortcode('divider', 'am_divider');


// ----- DropCap ----- //

function am_dropcap($atts, $content = null ) {
	return '
		<div class="dropcap">' . am_remove_wpautop($content) . '</div>';
}

add_shortcode('dropcap', 'am_dropcap');


// ----- Buttons ----- //

function am_buttons($atts, $content = null ) {
	extract(shortcode_atts( array(
		'color' => ''
	), $atts));
	return '
		<div class="button" style="background: ' . $color . '">' . am_remove_wpautop($content) . '</div>';
}

add_shortcode('button', 'am_buttons');


// ----- Collumns ----- //

/* ============= Two Columns ============= */

function am_collumn_one_half($atts, $content = null) {
   return '<div class="one_half">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_half', 'am_collumn_one_half' );

function am_collumn_one_half_last($atts, $content = null) {
   return '<div class="one_half last">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_half_last', 'am_collumn_one_half_last' );


/* ============= Three Columns ============= */

function am_collumn_one_third($atts, $content = null) {
   return '<div class="one_third">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_third', 'am_collumn_one_third' );

function am_collumn_one_third_last($atts, $content = null) {
   return '<div class="one_third last">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_third_last', 'am_collumn_one_third_last' );

function am_collumn_two_thirds($atts, $content = null) {
   return '<div class="two_third">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'two_thirds', 'am_collumn_two_thirds' );

function am_collumn_two_last($atts, $content = null) {
   return '<div class="two_third last">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'two_thirds_last', 'am_collumn_two_last' );

/* ============= Four Columns ============= */

function am_collumn_one_fourth($atts, $content = null) {
   return '<div class="one_fourth">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_fourth', 'am_collumn_one_fourth' );

function am_collumn_one_fourth_last($atts, $content = null) {
   return '<div class="one_fourth last">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'one_fourth_last', 'am_collumn_one_fourth_last' );

function am_collumn_three_fourths($atts, $content = null) {
   return '<div class="three_fourth">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'three_fourths', 'am_collumn_three_fourths' );

function am_collumn_three_fourths_last($atts, $content = null) {
   return '<div class="three_fourth last">' . am_remove_wpautop($content) . '</div>';
}
add_shortcode( 'three_fourths_last', 'am_collumn_three_fourths_last' );



// ----- Quotes ----- //

function am_quote( $atts, $content = null ) {  
    return '
		<blockquote>
			<p>' . am_remove_wpautop($content) . '</p>
		</blockquote>';  
}

add_shortcode('quote', 'am_quote');

// ----- Message Box ----- //
/*
Optional arguments:
 - Type: message, note, info, success, warning\

*/

function am_info_box($atts, $content = null ) {
	extract(shortcode_atts( array(
		'type' => 'message'
	), $atts));
	return '
		<div class="alert ' . $type . '">' . am_remove_wpautop($content) . '</div>';
}

add_shortcode('message', 'am_info_box');

// ----- YouTube Video ----- //

function am_youtube_video($atts, $content=null) {
    extract(shortcode_atts( array(
        'id'       => '',
        'width'    => '598',
        'height'   => '334',
        'title' => 'Check Out This Video'
    ), $atts));
    return '
		<div class="video">
			<iframe width="' . $width . '" height="' . $height .'" src="http://www.youtube.com/embed/' . $id . '&rel=0" frameborder="0" allowfullscreen></iframe>
			<h4>' . $title . '</h4>
		</div>';
}

add_shortcode('youtube', 'am_youtube_video');


// ----- Vimeo ----- //

function am_vimeo_video($atts, $content=null) {  
    extract(shortcode_atts( array(  
        'id'     => '',  
        'width'  => '598',  
        'height' => '334',  
        'color'  => 'BAC8D3',
		'title'  => 'Check Out This Video'
    ), $atts));  
    return '
		<div class="video">
			<iframe src="http://player.vimeo.com/video/' . $id . '?color=' . $color . '" width="' . $width .'" height="' . $height . '" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
			<h4>' . $title . '</h4>
		</div>';  
}  
add_shortcode('vimeo', 'am_vimeo_video');


// ===== Google Maps ===== //

function am__googleMaps($atts, $content = null) {  
   extract(shortcode_atts(array(  
      "width" => '598',  
      "height" => '334',  
      "src" => ''  
   ), $atts));  
   return '<div id="map"><iframe width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'"></iframe></div>';  
} 

add_shortcode("googlemap", "am__googleMaps");


// ===== Related Posts ===== //

function am_related_posts_shortcode( $atts ) {  
    extract(shortcode_atts(array(  
        'limit' => '5',  
    ), $atts));  
  
    global $wpdb, $post, $table_prefix;  
  
    if ($post->ID) {  
        $retval = '<ul>';  
        // Get tags  
        $tags = wp_get_post_tags($post->ID);  
        $tagsarray = array();  
        foreach ($tags as $tag) {  
            $tagsarray[] = $tag->term_id;  
        }  
        $tagslist = implode(',', $tagsarray);  
  
        // Do the query  
        $q = "SELECT p.*, count(tr.object_id) as count 
            FROM $wpdb->term_taxonomy AS tt, $wpdb->term_relationships AS tr, $wpdb->posts AS p WHERE tt.taxonomy ='post_tag' AND tt.term_taxonomy_id = tr.term_taxonomy_id AND tr.object_id  = p.ID AND tt.term_id IN ($tagslist) AND p.ID != $post->ID 
                AND p.post_status = 'publish' 
                AND p.post_date_gmt < NOW() 
            GROUP BY tr.object_id 
            ORDER BY count DESC, p.post_date_gmt DESC 
            LIMIT $limit;";  
  
        $related = $wpdb->get_results($q);  
        if ( $related ) {  
            foreach($related as $r) {  
                $retval .= '<li><a title="'.wptexturize($r->post_title).'" href="'.get_permalink($r->ID).'">'.wptexturize($r->post_title).'</a></li>';  
            }  
        } else {  
            $retval .= ' 
    <li>No related posts found</li>';  
        }  
        $retval .= '</ul>';  
        return $retval;  
    }  
    return;  
}

add_shortcode('related_posts', 'am_related_posts_shortcode');


// ===== Paypal Donation Link ===== //

function am_donate_shortcode( $atts ) {  
    extract(shortcode_atts(array(  
        'text' => 'Make a donation',  
        'account' => 'REPLACE ME',  
        'for' => '',  
    ), $atts));  
  
    global $post;  
  
    if (!$for) $for = str_replace(" ","+",$post->post_title);  
  
    return '<a class="donateLink" href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business='.$account.'&item_name=Donation+for+'.$for.'">'.$text.'</a>';  
  
}  
add_shortcode('donate', 'am_donate_shortcode');


// ===== tweetmeme ===== //

/*
Optional arguments:
 - link: specify URL directly
 - style: compact
 - source: username
 - float: none, left, right (default: left)

*/
function am_tweetmeme($atts, $content = null) {
   	extract(shortcode_atts(array(	'link' => '',
   									'style' => '',
   									'source' => '',
   									'float' => 'left'), $atts));
	$output = '';

	if ( $link == '' ) {
		global $post;
		$link = get_permalink( $post->ID );
	}

	if ( $link )
		$output .= "tweetmeme_url = '".$link."';";

	if ( $style )
		$output .= "tweetmeme_style = 'compact';";

	if ( $source )
		$output .= "tweetmeme_source = '".$source."';";

	if ( $link OR $style )
		$output = '<script type="text/javascript">'.$output.'</script>';

	$output .= '<div class="am-tweetmeme '.$float.'"><script type="text/javascript" src="http://tweetmeme.com/i/scripts/button.js"></script></div>';
	return $output;

}
add_shortcode( 'tweetmeme', 'am_tweetmeme' );