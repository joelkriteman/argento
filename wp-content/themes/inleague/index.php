<?php get_header(); ?>

	<?php // If post are found ?>
	<?php if ( have_posts() ) : ?>
	
		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading"><?php echo of_get_option('blog_title', 'The Blog'); ?></h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">
			
				<?php get_template_part( 'loop', 'posts' ); ?>
	
	<?php // if there are no post show error ?>
	<?php else : ?>
	
		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading"><?php _e( 'Nothing Found', 'inLEAGUE' ); ?></h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">
			
				<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords using the searchform above.', 'inLEAGUE' ); ?></p>

<?php endif; ?>

			    
			    <div id="pagination">
			    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<div id="prev"><?php next_posts_link( __( '&larr; Previous', 'inLEAGUE' ) ); ?></div>
					<div id="prev"><?php previous_posts_link( __( 'Next posts &rarr;', 'inLEAGUE' ) ); ?></div>
				<?php endif; ?>
				
				</div><!-- end #pagination -->
				
			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
