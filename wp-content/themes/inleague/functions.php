<?php

/* 
	Here we have all the custom functions for the theme
	Please be extremely cautious editing this file,
	When things go wrong, they tend to go wrong in a big way.
*/

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 590;

add_action( 'after_setup_theme', 'inleague_theme_setup' );

function inleague_theme_setup() {
	
	/* Add theme support for automatic feed links. */	
	add_theme_support( 'automatic-feed-links' );
	
	/* Add theme support for post thumbnails (featured images) */
	if ( function_exists( 'add_theme_support' ) ) { 
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size(600, 420, true); // sets width and height for thumbnail
		add_image_size( 'small-thumb', 200, 140, true);
		add_image_size( 'portfolio-thumb', 300, 200, true);
		add_image_size( 'slider-thumb', 940, 348, true);
	}
	
	// ===== ACTIONS ===== //
	
	/* Add your nav menus function to the 'init' action hook. */
	add_action('init', 'am_register_custom_menus');
	
	/* Add your sidebars function to the 'widgets_init' action hook. */
	add_action('widgets_init', 'am_register_sidebar');
	
	/* Load and enqueue all our javascript files */
	add_action('wp_enqueue_scripts', 'am_load_javascript');
	
	// ===== FILTERS ===== //
	
	/* Custom Searchform */
	add_filter( 'get_search_form', 'am_search_form' );
	
} // end inleague_theme_setup


/* Custom menus function */
function am_register_custom_menus() {
  register_nav_menus(
    array( 'header-menu' => __( 'Header Menu' ))
  );
}

/* Sidebars function */
function am_register_sidebar() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'inLEAGUE' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The primary widget area', 'inLEAGUE' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'inLEAGUE' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The secondary widget area', 'inLEAGUE' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title"><span>',
		'after_title' => '</span></h3>',
	) );

	// Area 3, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'First Footer Widget Area', 'inLEAGUE' ),
		'id' => 'first-footer-widget-area',
		'description' => __( 'The first footer widget area', 'inLEAGUE' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h4 class="widget-title"><span>',
		'after_title' => '</span></h4>',
	) );

	// Area 4, located in the footer. Empty by default.
	register_sidebar( array(
		'name' => __( 'Second Footer Widget Area', 'inLEAGUE' ),
		'id' => 'second-footer-widget-area',
		'description' => __( 'The second footer widget area', 'inLEAGUE' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h4 class="widget-title"><span>',
		'after_title' => '</span></h4>',
	) );

	// Area 5, located in the footer. Archives by default.
	register_sidebar( array(
		'name' => __( 'Third Footer Widget Area', 'inLEAGUE' ),
		'id' => 'third-footer-widget-area',
		'description' => __( 'The third footer widget area', 'inLEAGUE' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title"><span>',
		'after_title' => '</span></h4>',
	) );

	// Area 6, located in the footer. Categories by default.
	register_sidebar( array(
		'name' => __( 'Fourth Footer Widget Area', 'inLEAGUE' ),
		'id' => 'fourth-footer-widget-area',
		'description' => __( 'The fourth footer widget area', 'inLEAGUE' ),
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title"><span>',
		'after_title' => '</span></h4>',
	) );
}


/* searchform function */
function am_search_form( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    		    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search..." />
    		</form>';

    return $form;
}

/* javascript enqueue function */
function am_load_javascript() {
	// Register Cufon
	wp_register_script('cufon', get_template_directory_uri() . '/js/cufon.js', array(), '1.09i', true);
	// Register all scripts
	wp_register_script('all-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', true );
	//Register plugins
	wp_register_script('plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0', true);
	
	wp_enqueue_script('cufon');
	wp_enqueue_script('plugins');
	wp_enqueue_script('all-scripts');
	
}   


// ===== INCLUDES ===== //
// ===== Custom post types ===== //


/* Shortcodes */
include 'lib/shortcodes.php';

/* Theme Options */
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
	require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}


// ===== OTHERS ===== //

/* Excerpt length */
function in_custom_excerpt($length){
	global $post;
	$content = strip_tags($post->post_content);
	preg_match('/^\s*+(?:\S++\s*+){1,'.$length.'}/', $content, $matches);
	echo "<p>" . $matches[0] . "" . continue_reading_link() . "</p>";
}


function continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'inLEAGUE' ) . '</a>';
}

/* Comments */
if ( ! function_exists( 'inLEAGUE_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own inLEAGUE_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Ten 1.0
 */
function inLEAGUE_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <div class="comment" id="comment-<?php comment_ID(); ?>">
            <div class="avatar">
            	<?php echo get_avatar( $comment, 50 ); ?>
            </div><!-- end .avatar -->
            <div class="commentContent">
            	<p>
            		<strong><?php printf( __( '%s', 'inLEAGUE' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?></strong>
            		<br />
            		<span class="date">
            			<?php
            			    /* translators: 1: date, 2: time */
            			    printf( __( '%1$s at %2$s', 'inLEAGUE' ), get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( __( '(Edit)', 'inLEAGUE' ), ' ' );
            			?>
            		</span>
            	</p>
            	
            	<?php comment_text(); ?>
            	
            	<div class="reply">
                    <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                </div><!-- .reply -->
            	
            	<?php if ( $comment->comment_approved == '0' ) : ?>
                    <em><?php _( 'Your comment is awaiting moderation.', 'inLEAGUE' ); ?></em>
                    <br />
                <?php endif; ?>
            </div><!-- .commentContent -->
        </div><!-- end .comment -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'inLEAGUE' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'inLEAGUE'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}
endif;

/* related posts */
function related_portfolio_posts() {
	
	$post_type = 'portfolio';
	$tax = 'filter';
	$post_count = 3;
	$counter = 0;
	$tax_terms = get_terms($tax);
	if ($tax_terms) {

		echo '<h2>Related Work</h2>
				<div id="portfolio">
					<div id="relatedThumbs">';
	
		foreach ($tax_terms as $tax_term) {
		$args = array(
			'post_type' => $post_type,
			"$tax" => $tax_term->slug,
			'post_status' => 'publish'
		);
	
		$my_query = null;
		$my_query = new WP_Query($args);
		
		if( $my_query->have_posts() ) {
								
			while ($my_query->have_posts() && $counter < $post_count) : $my_query->the_post();
			
			$terms = get_the_terms( get_the_ID(), 'filter' );
			
			echo '<div class="logo identity">
                    <div class="imageWraper">
                    	<a href="' . get_permalink() . '">';
                    	if (has_post_thumbnail()) {
	                        the_post_thumbnail( 'portfolio-thumb' );
	                    }
	                    else {
	                        echo
	                            '<img src="',
	                            get_template_directory_uri(), '/images/griver-thm.jpg',
	                            '" alt="thumbnail" />';
	                    }
             echo '		<span></span>
                    	</a>
                    </div><!--  .imageWrap -->
                    <a href="' . get_permalink() . '">' . get_the_title() . '</a>
                    <span>';
                    foreach ($terms as $term) { echo strtolower(preg_replace('/\s+/', '-', $term->name)). '/ '; }
             echo '</span>
                </div>';
		
			$counter += 1;
			endwhile;
	
	    }
	    
	    wp_reset_query();
		
		}
		
	    echo '</div></div><!-- end #portfolio -->';
		
	}
	
}

	/*-------------------------------------------------------------------------*/
/* Change Standard WordPress Admin Greeting */
/*-------------------------------------------------------------------------*/
add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );
function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
$profile_url = get_edit_profile_url( $user_id );
if ( 0 != $user_id ) {
/* Add the "My Account" menu */
$avatar = get_avatar( $user_id, 28 );
$howdy = sprintf( __('Hello %1$s'), $current_user->display_name );
$class = empty( $avatar ) ? '' : 'with-avatar';
$wp_admin_bar->add_menu( array(
'id' => 'my-account',
'parent' => 'top-secondary',
'title' => $howdy . $avatar,
'href' => $profile_url,
'meta' => array(
'class' => $class,
),
) );
}
}