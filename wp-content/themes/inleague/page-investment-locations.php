 <?php
/*
 * Template Name: Investment Locations
 *
 */
?>

<?php get_header() ?>


<div id="sliderWraper" class="container_12">
	<div id="sliderContainer" class="grid_12">

			
					<img src="<?php bloginfo('template_directory'); ?>/images/Locations-Header.jpg" alt="" />

	</div><!-- end #sliderContainer -->
</div><!-- end #sliderWraper -->



<div class="one_half">
  <h4>Why Edinburgh?</h4>
  <?php the_field('left_column'); ?>
</div>

<div class="one_half last">
  <h4>Why Dundee?</h4>
  <?php the_field('right_column'); ?>
</div>



<?php get_footer() ?>