<?php

$category = of_get_option('slider_select_categories');
$number = of_get_option('slider_posts');

?>
<div id="sliderWraper" class="container_12">
	<div id="sliderContainer" class="grid_12">
		<div id="slider" class="nivoSlider">
			<?php
			$postCategory = new WP_Query();
			$postCategory->query(
				'cat=' . $category . '&showposts=' . $number . ''
			);
			
			if ($postCategory->have_posts()) : while ($postCategory->have_posts()) : $postCategory->the_post(); ?>
			
					<?php the_post_thumbnail('slider-thumb'); ?>
			
			<?php endwhile; endif; wp_reset_query(); ?>
			<!--<img src="images/slider-1.jpg" alt="slider-1" width="940" height="348" />-->
		</div><!-- end #slider -->
	</div><!-- end #sliderContainer -->
</div><!-- end #sliderWraper -->