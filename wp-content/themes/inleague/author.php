<?php get_header(); ?>

<?php
	if ( have_posts() ) the_post(); ?>
	
		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
				<?php printf( __( 'Author Archives: ', 'inLEAGUE') ); ?>
				<span>
					<?php printf( __( '%s', 'inLEAGUE' ),  get_the_author() ); ?>
				</span>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="page" class="container_12">
			<section id="content" role="main" class="grid_8">

				<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
			    <div id="post-author">
				    <div class="avatar">
				    	<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'author_bio_avatar_size', 100 ) ); ?>
				    </div><!-- end .avatar -->
				    <div class="author-desc">
				    	<strong><?php printf( esc_attr__( 'About %s', 'inLEAGUE' ), get_the_author() ); ?></strong><br />
				    	<?php the_author_meta( 'description' ); ?>
				    </div><!-- .end .author-desc -->
				</div><!-- end .post-author -->
				<?php endif; ?>
	

<?php rewind_posts(); get_template_part( 'loop', 'alternate' );?>

			    <div id="pagination">
			    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<div id="prev"><?php next_posts_link( __( '&larr; Previous', 'inLEAGUE' ) ); ?></div>
					<div id="prev"><?php previous_posts_link( __( 'Next posts &rarr;', 'inLEAGUE' ) ); ?></div>
				<?php endif; ?>
				</div><!-- end #pagination -->
				
			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
