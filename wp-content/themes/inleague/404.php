<?php get_header(); ?>
				
	<div id="pageHeader" class="container_12">
		<div id="pageTitle" class="grid_8">
			<h1 class="heading"><?php echo of_get_option('fourohfour', 'WOOPS... SOMETHING\'S WRONG'); ?></h1>
		</div><!-- end #pageTitle -->
		<div id="topSearch" class="grid_4">
			<?php get_search_form(); ?>
		</div><!-- end #topSearch -->
		<div class="divider grid_12"></div>
	</div><!-- end #pageHeader -->
	
	<div id="pageNotFound" class="container_12">
		<section class="grid_12">
			<img src="<?php echo get_template_directory_uri(); ?>/images/404.png" alt="404" width="347" height="260" />
			<h2><?php _e('IT SEEMS THE PAGE YOU WERE LOOKING FOR DOESNT EXIST', 'inLEAGUE') ?></h2>
		</section>
	</div><!-- end #404 -->

<?php get_footer(); ?>