<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title>Argento Property<?php wp_title( '|', true, 'left' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon" /> 
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34909407-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    
    <!-- nivo slider styles -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/nivo-slider/nivo-slider.css" type="text/css" media="screen" />
	
	<?php wp_head(); ?>
	
<style>

/* Text Selection */

::selection { background: <?php echo of_get_option('main_color', '#0a95dd') ?>; /* Safari */ }
::-moz-selection { background: <?php echo of_get_option('main_color', '#0a95dd') ?>; /* Firefox */ }

/* Everything else */

body {
	border-top: 5px solid <?php echo of_get_option('main_color', '#0a95dd') ?>;
<?php
	$background = of_get_option('background_image');
	if ($background) {
		echo 'background:url(' .$background['image']. ') ' . $background['color'] . ' ' . $background['repeat'] . ' ' . $background['position'] . ' ' . $background['attachment'] . '';
	}
?>
}

a { color: <?php echo of_get_option('link_color', '#0a95dd') ?>; }
a:hover,
#footerWraper .widget-container a:hover { color: <?php echo of_get_option('link_hover_color', '#333333') ?>; }

em,
#recentWork li > a:hover,
#portfolio li > a:hover,
#portfolio ul#filter li.current a,
.testimonialWraper .testimonial p:before,
#footerWraper .widget-container .footPost .footMeta > a:hover,
blockquote:before { color: <?php echo of_get_option('main_color', '#0a95dd') ?>; }

nav#main li.current_page_item a,
header nav#main li a:hover {
color: #333333;
}

#recentWork .imageWraper span,
#portfolio .imageWraper span { background-color: <?php echo of_get_option('main_color', '#0a95dd') ?>; }

#comments .bypostauthor { border-left: 3px solid <?php echo of_get_option('main_color', '#0a95dd') ?> !important; }

.tabs .selectors li.active { border-top: 3px solid <?php echo of_get_option('main_color', '#0a95dd') ?>; }

.button, input[type=submit] { background: <?php echo of_get_option('main_color', '#0a95dd') ?>; }


</style>

<?php
	if (of_get_option('custom_css')) {
		echo '<style>';
		echo of_get_option('custom_css');
		echo '</style>';
	}
?>

<?php if( is_front_page()) {
	echo '<style>
			header { margin-bottom: 60px; }
		  </style>';

}
?>
	
</head>

<body <?php body_class(); ?>>
	<div id="wraper" class="clearfix">
		<header class="container_12">
		

		
			<div id="logo" class="grid_3">
				
				<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
				
					<?php if(of_get_option('logo_image')) { ?>
						<img src="<?php echo of_get_option('logo_image'); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php } else { ?>
					
					<img src="<?php get_template_directory_uri(); ?>/images/logo.jpg" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					
					<?php } ?>
					
				</a>
				
			</div><!-- end #logo -->
			<nav id="main" role="navigation" class="grid_9">
          <img src="<?php bloginfo('template_directory'); ?>/images/Argento-logo-2.png" alt="" id="logo2" />
          <span class="headerphone">0131 242 0009</span>
				<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
				
				
			</nav><!-- end nav -->
			<div class="clear"></div>
		</header><!-- end header -->
