 <?php
/*
 * Template Name: Property Development
 *
 */
?>

<?php get_header() ?>

	
	<div class="clear"></div>
	
			<div class="margintwenty">
		  <img src="<?php the_field('header_image'); ?>" alt="" />
		</div>

<div class="clear"></div>

  		<div class="margintwenty">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
      </div>

<div class="clear"></div>
<hr />

<div class="one_third">
  <h4><?php the_field('left_title'); ?></h4>
  <?php the_field('left_text'); ?>
</div>

<div class="one_third">
  <h4><?php the_field('centre_title'); ?></h4>
  <?php the_field('centre_text'); ?>
</div>

<div class="one_third last">
  <h4><?php the_field('right_title'); ?></h4>
  <?php the_field('right_text'); ?>
</div>

<div class="clear"></div>



<div class="margintwenty">
  <h4><?php the_field('bottom_title'); ?></h4>
  <?php the_field('bottom_text'); ?>
</div>

<hr />

<?php
    $args = array( 'category' => 6, 'post_type' =>  'post', 'numberposts' =>100 ); 
    $postslist = get_posts( $args );    
    foreach ($postslist as $post) :  setup_postdata($post); 
    ?>
    <div class="blogPost">
      <div class="post-thumbnail small-thm">
        <a href="<?php the_field('ppimage1'); ?>" rel="lightbox[<?php the_title(); ?>]"><img src="<?php the_field('ppimage1'); ?>" /></a>
      </div>
      <div class="title"><?php the_title(); ?></div>
                <div class="excerpt">
                  <?php the_content(); ?>
                  <?php 
										  if( get_field('ppimage1') ):
	?><a href="<?php the_field('ppimage1'); ?>" rel="lightbox[<?php the_title(); ?>]" class="readmore">VIEW MORE IMAGES</a><?php
endif;
 
?>

                </div>
                           
               

<?php 
										  if( get_field('ppimage2') ):
	?><a href="<?php the_field('ppimage2'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>


<?php 
										  if( get_field('ppimage3') ):
	?><a href="<?php the_field('ppimage3'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage4') ):
	?><a href="<?php the_field('ppimage4'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>


<?php 
										  if( get_field('ppimage5') ):
	?><a href="<?php the_field('ppimage5'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage6') ):
	?><a href="<?php the_field('ppimage6'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage7') ):
	?><a href="<?php the_field('ppimage7'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage8') ):
	?><a href="<?php the_field('ppimage8'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage9') ):
	?><a href="<?php the_field('ppimage9'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage10') ):
	?><a href="<?php the_field('ppimage10'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage11') ):
	?><a href="<?php the_field('ppimage11'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage12') ):
	?><a href="<?php the_field('ppimage12'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage13') ):
	?><a href="<?php the_field('ppimage13'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage14') ):
	?><a href="<?php the_field('ppimage14'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage15') ):
	?><a href="<?php the_field('ppimage15'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage16') ):
	?><a href="<?php the_field('ppimage16'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage17') ):
	?><a href="<?php the_field('ppimage17'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage18') ):
	?><a href="<?php the_field('ppimage18'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage19') ):
	?><a href="<?php the_field('ppimage19'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage20') ):
	?><a href="<?php the_field('ppimage20'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage21') ):
	?><a href="<?php the_field('ppimage21'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

<?php 
										  if( get_field('ppimage22') ):
	?><a href="<?php the_field('ppimage22'); ?>" rel="lightbox[<?php the_title(); ?>]"></a><?php
endif;
 
?>

               
               
                
      </div>
       
   
    <?php endforeach; ?>


<?php get_footer() ?>