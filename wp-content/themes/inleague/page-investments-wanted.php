 <?php
/*
 * Template Name: Investments - Wanted
 *
 */
?>

<?php get_header() ?>

	
	<div class="clear"></div>
	
	<div id="pageintro">
	   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
	</div>
	
	<hr>
	

	
<div id="investmentswanted">
<?php
    $args = array( 'category' => 31, 'post_type' =>  'post', 'numberposts' =>100 ); 
    $postslist = get_posts( $args );    
    foreach ($postslist as $post) :  setup_postdata($post); 
    ?>
    <div class="blogPost">
      <div class="post-thumbnail small-thm">
        <?php the_post_thumbnail(); ?>
      </div>
      
      <div class="availabletop">
        <div class="title"><?php the_title(); ?></div>
        <div class="price">Budget: <?php the_field('budget'); ?></div>
        <div class="price">Yield: <?php the_field('yield'); ?></div>
        <div class="price">Purpose: <?php the_field('purpose'); ?></div><br /><br />
        Type: <?php the_field('type'); ?><br /><br />
        Area: <?php the_field('area'); ?><br />
        
      </div>

      
      <div class="excerpt" id="availablecontent"><?php the_excerpt(); ?>
      </div>
      
      </div>
       
   
    <?php endforeach; ?>
</div>

<?php get_footer() ?>