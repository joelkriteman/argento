 <?php
/*
 * Template Name: Full Width
 *
 */
?>

<?php get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<!--<div id="pageHeader" class="container_12">-->	
			<div id="pageTitle " class="grid_8">
				<h1 class="heading">
					<?php the_title(); ?>
				</h1>
			</div><!-- end #pageTitle -->
		<!--	<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<!--<div class="divider grid_12"></div>-->			
		<!--</div><!-- end #pageHeader -->
		
		<div id="page" class="container_12">
			<section id="content" role="main" class="grid_121" style="margin-top:-10px;">

				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'inLEAGUE' ), 'after' => '' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'inLEAGUE' ), '', '' ); ?>

				<?php // uncomment the line below if you want comments in full-width pages ?>
				<?php //comments_template( '', true ); ?>
				
			</section><! end #content -->
		</div><!-- end #page -->

<?php endwhile; ?>

<?php get_footer(); ?>