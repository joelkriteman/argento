 <?php
/*
 * Template Name: Argento home
 *
 */
?>

<?php get_header() ?>


		<?php
		
		
		// Slider
		if ( of_get_option('home_show_slider') ) { include("home_slider.php"); }
		
		// Slogan
		if ( of_get_option('home_show_slogan') ) { include("home_slogan.php"); }
		
		
		// Services
		if ( of_get_option('home_show_services') ) { include("home_services.php"); }
		
		?>

<div class="clear"></div>

<div id="recentWork" class="container_12">
	<section class="grid_12">
		<ul>
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/what-we-do" title="What We Do" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-What-We-Do.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/what-we-do" title="What We Do" rel="bookmark">What We Do</a>
			</li>
			
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/investments-for-sale" title="Investments for Sale" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-Available-Property.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/investments-for-sale" title="Available Investments" rel="bookmark">Investments For Sale</a>
			</li>
			
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/investment-locations" title="Investment Locations" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-Investment-Locations.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/investment-locations" title="Investment Locations" rel="bookmark">Investment Locations</a>
			</li>
			
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/property-development" title="Previous Projects" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-Previous-Projects.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/property-development" title="Previous Projects" rel="bookmark">Property Development</a>
			</li>
			
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/news" title="News" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-News.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/news" title="News" rel="bookmark">News</a>
			</li>
			
			<li>
				<div class="imageWraper">
					<a href="<?php bloginfo('home'); ?>/contact" title="Contact" rel="bookmark">
						<img src="<?php bloginfo('template_directory'); ?>/images/Argento-Contact-Us.jpg" alt="" />
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php bloginfo('home'); ?>/contact" title="Contact" rel="bookmark">Contact Us</a>
			</li>
			
		</ul>
	</section>
</div><!-- end #recentWork -->

<script>
// ----- Nivo Slider ----- //

jQuery(window).load(function() {
    jQuery('#slider').nivoSlider({
        effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
        slices: 15, // For slice animations
        boxCols: 8, // For box animations
        boxRows: 4, // For box animations
        animSpeed: 1500, // Slide transition speed
        pauseTime: 5000, // How long each slide will show *3000
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: <?php if ( of_get_option('home_show_slider_arrows') ) { echo 'true'; } else { echo 'false'; } ?>, // Next & Prev navigation
        directionNavHide: true, // Only show on hover
        controlNav: <?php if ( of_get_option('home_show_slider_control') ) { echo 'true'; } else { echo 'false'; } ?>,// 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        controlNavThumbsFromRel: false, // Use image rel for thumbs
        controlNavThumbsSearch: '.jpg', // Replace this with...
        controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
        keyboardNav: true, // Use left & right arrows
        pauseOnHover: true, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        captionOpacity: 0, // Universal caption opacity
        prevText: 'Prev', // Prev directionNav text
        nextText: 'Next', // Next directionNav text
        randomStart: false, // Start on a random slide
        beforeChange: function(){}, // Triggers before a slide transition
        afterChange: function(){}, // Triggers after a slide transition
        slideshowEnd: function(){}, // Triggers after all slides have been shown
        lastSlide: function(){}, // Triggers when last slide is shown
        afterLoad: function(){} // Triggers when slider has loaded
    });
});
</script>

<?php get_footer() ?>