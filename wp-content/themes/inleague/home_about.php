<div id="aboutTestimonials" class="container_12">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php if ( of_get_option('home_show_about') && of_get_option('home_show_testimonials') ) : ?>

	<section class="grid_8">
		<h1 class="heading"><span><?php echo of_get_option('home_about_heading', 'A Little About Us') ?></span></h1>
		
		<?php
		
			the_content();
			
			edit_post_link( __( 'Edit', 'inLEAGUE' ), '', '' );
		
		?>
		
	</section><!-- end aboutUs -->
	<section class="grid_4">
		<h1 class="heading"><span>Testimonials</span></h1>
		<ul id="testimonials">
		
		<?php 
		
			$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 6 );  
			$loop = new WP_Query( $args );  
				while ( $loop->have_posts() ) : $loop->the_post();
				
			$custom = get_post_custom($post->ID);
			$name = $custom['name'][0];
			$company = $custom['company'][0];
			$website = $custom['website'][0];

		?>
		<li>
			<div class="testimonialWraper">
				<div class="testimonial">
					<?php the_content(); ?>
				</div>
				<div class="testifier">
					<p><?php the_title(); ?></p>
					<?php
						if ( $website == '' || $website == 'http://') {
							echo "<p> $company </p>";
						} else {
							echo '<p><a rel="external" href="' . $website . '">' . $company . '</a></p>';
						}
					?>
				</div><!-- end .testimonial -->
			</div><!-- end .testimonialWraper -->
		</li>	
		<?php endwhile; ?>
		</ul>
		
	</section>
	
	<?php elseif ( of_get_option('home_show_about') ) :  ?>
	
	<section class="grid_12">
		<h1 class="heading"><span><?php echo of_get_option('home_about_heading', 'A Little About Us') ?></span></h1>
		
		<?php
		
			the_content();
			
			edit_post_link( __( 'Edit', 'inLEAGUE' ), '', '' );
		
		?>
		
	</section><!-- end aboutUs -->
	
	<?php endif; ?>
	
</div><!-- end #aboutTestimonials -->

<?php endwhile; ?>