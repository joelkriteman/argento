 <?php
/*
 * Template Name: What We do
 *
 */
?>

<?php get_header() ?>


		<?php
		
		
		// Slider
		if ( of_get_option('home_show_slider') ) { include("home_slider.php"); }

		
		?>

<div class="clear"></div>

	<div id="homeSlogan" class="container_16">
		<div id="sloganText" class="grid_12">
			<p><?php the_field('intro_text'); ?></p>
		</div><!-- end #sloganText -->		
	</div><!-- end #homeSlogan -->
	
	<a href="<?php bloginfo('home'); ?>/?page_id=142"><img src="<?php bloginfo('template_directory'); ?>/images/Argento-Signup-Flash.png" alt="" id="whatflash" /></a>

<div class="clear"></div>
<hr />

<div class="one_third">
  <h4>Source</h4>
  <?php the_field('source'); ?>
</div>

<div class="one_third">
  <h4>Fund</h4>
  <?php the_field('fund'); ?>
</div>

<div class="one_third last">
  <h4>Buy</h4>
  <?php the_field('buy'); ?>
</div>

<div class="clear"></div>

<div class="one_third">
  <h4>Develop</h4>
  <?php the_field('develop'); ?>
</div>

<div class="one_third">
  <h4>Manage</h4>
  <?php the_field('manage'); ?>
</div>

<div class="one_third last">
  <h4>Additional Services</h4>
  <?php the_field('additional_services'); ?>
</div>

<script>
// ----- Nivo Slider ----- //

jQuery(window).load(function() {
    jQuery('#slider').nivoSlider({
        effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
        slices: 15, // For slice animations
        boxCols: 8, // For box animations
        boxRows: 4, // For box animations
        animSpeed: 1500, // Slide transition speed
        pauseTime: 5000, // How long each slide will show *3000
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: <?php if ( of_get_option('home_show_slider_arrows') ) { echo 'true'; } else { echo 'false'; } ?>, // Next & Prev navigation
        directionNavHide: true, // Only show on hover
        controlNav: <?php if ( of_get_option('home_show_slider_control') ) { echo 'true'; } else { echo 'false'; } ?>,// 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        controlNavThumbsFromRel: false, // Use image rel for thumbs
        controlNavThumbsSearch: '.jpg', // Replace this with...
        controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
        keyboardNav: true, // Use left & right arrows
        pauseOnHover: true, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        captionOpacity: 0, // Universal caption opacity
        prevText: 'Prev', // Prev directionNav text
        nextText: 'Next', // Next directionNav text
        randomStart: false, // Start on a random slide
        beforeChange: function(){}, // Triggers before a slide transition
        afterChange: function(){}, // Triggers after a slide transition
        slideshowEnd: function(){}, // Triggers after all slides have been shown
        lastSlide: function(){}, // Triggers when last slide is shown
        afterLoad: function(){} // Triggers when slider has loaded
    });
});
</script>

<?php get_footer() ?>