<?php if ( of_get_option('home_show_slogan') && of_get_option('home_show_slogan_button') ) : ?>

<div class="one_half">

	<div id="homeSlogan">
		<div id="sloganText">
			<p><?php echo of_get_option('home_slogan_text'); ?></p>
		</div><!-- end #sloganText -->		
	</div><!-- end #homeSlogan -->

<?php elseif( of_get_option('home_show_slogan') ) : ?>

	<div id="homeSlogan">
		<div id="sloganText">
			<p><?php echo of_get_option('home_slogan_text'); ?></p>
		</div><!-- end #sloganText -->
	</div><!-- end #homeSlogan -->

<?php endif; ?>

<a href="<?php bloginfo('home'); ?>/contact"><img src="<?php bloginfo('template_directory'); ?>/images/Argento-Signup-Flash.png" alt="" id="homeflash" /></a>

</div>