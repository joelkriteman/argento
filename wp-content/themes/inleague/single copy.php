<?php get_header(); ?>

<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php _e('The Blog', 'inLEAGUE'); ?>
						<span>/
							<?php // only show the first category ?>
							<?php
								$category = get_the_category(); 
								echo $category[0]->cat_name;
							?>
						</span>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">
			
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    
			    <div id="post-<?php the_ID(); ?>" <?php post_class('blogPost singlePost'); ?>>

                    <div class="post-thumbnail">
                    
                    	<?php 
                            if (has_post_thumbnail()) {
                                the_post_thumbnail();
                            }
                            else {
                                echo
                                    '<img src="',
                                    get_template_directory_uri(), '/images/post1.jpg',
                                    '" alt="thumbnail" />';
                            }
                        ?>
                    
                    </div><!-- end .post-thumbnail -->
                    
                    <article class="post">
                    
                    	<div class="post-meta">
                    
                    		<h1><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                    
                    		<span>by <?php the_author_posts_link(); ?> | <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>
                <?php edit_post_link( __( 'Edit', 'inLEAGUE' ), '| ', '' ); ?></span>
                    
                    	</div><!-- end .post-meta -->
                    
                    	<div class="post-content">
	                    	
	                    	<?php the_content(); ?>
                    
                    	</div><!-- end .post-content -->
                    
                    </article><!-- end .post -->
                
                </div><!-- end .blogPost -->

                <?php endwhile; endif; ?>			    
			    
			    <?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
			    <div id="post-author">
				    <div class="avatar">
				    	<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'author_bio_avatar_size', 50 ) ); ?>
				    </div><!-- end .avatar -->
				    <div class="author-desc">
				    	<strong><?php printf( esc_attr__( 'About %s', 'inLEAGUE' ), get_the_author() ); ?></strong><br />
				    	<?php the_author_meta( 'description' ); ?>
				    	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
							<?php printf( __( 'View all posts by %s &rarr;', 'inLEAGUE' ), get_the_author() ); ?>
						</a>

				    </div><!-- .end .author-desc -->
				</div><!-- end .post-author -->
				<?php endif; ?>
									
			    
			    <?php comments_template( '', true ); ?>
			    				
			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>