<?php 
// Options variables 
$number = of_get_option('home_recent_work_posts');
?>
<div id="recentWork" class="container_12">
	<section class="grid_12">
		<h1 class="heading"><span><?php echo of_get_option('home_recent_work_text') ?></span></h1>
		<ul>
		<?php 
		
			$args = array( 'post_type' => 'portfolio', 'posts_per_page' =>  $number );  
			$loop = new WP_Query( $args );  
				while ( $loop->have_posts() ) : $loop->the_post();
				
			$terms = get_the_terms( get_the_ID(), 'filter' );

		?>
			<li>
				<div class="imageWraper">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark">
						<?php the_post_thumbnail( 'portfolio-thumb' ); ?>
						<span></span>
					</a>
				</div><!-- end .imageWrap -->
				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'inLEAGUE' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				<span>
					<?php
						foreach ($terms as $term) {
							echo strtolower(preg_replace('/\s+/', '-', $term->name)). '/ ';
						}
					?>
				</span>
			</li>
			
		<?php endwhile; ?>
		</ul>
	</section>
</div><!-- end #recentWork -->