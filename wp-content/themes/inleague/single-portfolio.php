<?php get_header(); ?>

<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php _e('Portfolio', 'inLEAGUE'); ?>
						<span>/ <?php echo of_get_option('single_portfolio', 'Case Study'); ?></span>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="caseStudy" class="container_12">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="largeImage" id="post-<?php the_ID(); ?>" <?php post_class('grid_8'); ?>>
				<!--<img src="images/portfolio/leopard-big.jpg" alt="leopard" />-->
			</div><!-- end #largeImage -->
			<section class="grid_4">
				<div id="moreShots">
					<h2>More Shots</h2>
					<ul>
						<?php
							$args = array(
							   'post_type' => 'attachment',
							   'numberposts' => -1,
							   'post_status' => null,
							   'post_parent' => $post->ID
							  );
							
							  $attachments = get_posts( $args );
							     if ( $attachments ) {
							        foreach ( $attachments as $attachment ) {
							        	$large_image =  wp_get_attachment_image_src( $attachment->ID, 'fullsize', false, '' );
										echo '<li>';
										echo '<a class="thmSelector" href="' . $large_image[0] . '">';
										echo wp_get_attachment_image( $attachment->ID, array(74,74) );
										echo '</a></li>';
									}
							     }
						?>
					</ul>
				</div><!-- end #moreShots -->
				<div class="clear"></div>
				<div id="itemDesc">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<ul class="arrow">
						<?php $terms = get_the_terms( get_the_ID(), 'filter' ); ?>
						<?php
							foreach ($terms as $term) {
								echo '<li>';
								echo strtolower(preg_replace('/\s+/', '-', $term->name)). ' ';
								echo '</li>';
							}
						?>
					</ul>
					
					<?php
						$custom = get_post_custom($post->ID);
						$website = $custom['website'][0];
					
					if ( $website == '' || $website == 'http://') {
						// do nothig here
					} else {
						echo '<a class="button" rel="external" href="' . $website . '">Visit Site</a>';
					}
					?>
				</div><!-- end #itemDesc -->
			</section>
			
			<div id="relatedWork" class="grid_12">
				<section>
				
					<?php related_portfolio_posts(); ?>
					
					<div id="casePagination">
						<div id="prev"><?php next_post_link('%link', __( '&larr; Previous', 'inLEAGUE' ) ); ?></div>
						<div id="next"><?php previous_post_link('%link', __( 'Next &rarr;', 'inLEAGUE' ) ); ?></div>
					</div><!-- end #casePagination -->
				</section>			
			</div><!-- end #relatedWork -->
			
			<?php endwhile; endif; ?>
			
		</div><!-- end #caseStudy -->
		
<?php //get_sidebar(); ?>
<?php get_footer(); ?>