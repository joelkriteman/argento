<?php get_header(); ?>

<?php if ( have_posts() ) the_post(); ?>

		<div id="pageHeader" class="container_12">
			<div id="pageTitle" class="grid_8">
				<h1 class="heading">
					<?php if ( is_day() ) : ?>
						<?php printf( __( 'Daily Archives:', 'inLEAGUE' ) ) ?><span><?php printf( __(' %s ', 'inLEAGUE'), get_the_date() ); ?></span>
					<?php elseif ( is_month() ) : ?>
						<?php printf( __( 'Monthly Archives:', 'inLEAGUE' ) ) ?><span><?php printf( __(' %s ', 'inLEAGUE'),get_the_date('F Y') ); ?></span>
					<?php elseif ( is_year() ) : ?>
						<?php printf( __( 'Yearly Archives: ', 'inLEAGUE' ) ) ?><span><?php printf( __(' %s ', 'inLEAGUE'), get_the_date('Y') ); ?></span>
					<?php else : ?>
						<?php _e( 'Blog Archives', 'inLEAGUE' ); ?>
					<?php endif; ?>
				</h1>
			</div><!-- end #pageTitle -->
			<div id="topSearch" class="grid_4">
				<?php get_search_form(); ?>
			</div><!-- end #topSearch -->
			<div class="divider grid_12"></div>			
		</div><!-- end #pageHeader -->
		
		<div id="blog" class="container_12">
			<section id="content" role="main" class="grid_8">

				<?php rewind_posts(); get_template_part( 'loop', 'alternate' );?>

			    <div id="pagination">
			    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
					<div id="prev"><?php next_posts_link( __( '&larr; Previous', 'inLEAGUE' ) ); ?></div>
					<div id="prev"><?php previous_posts_link( __( 'Next posts &rarr;', 'inLEAGUE' ) ); ?></div>
				<?php endif; ?>
				</div><!-- end #pagination -->
				
			</section><!-- end #content -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
