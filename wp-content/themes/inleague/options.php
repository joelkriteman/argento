<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';
	
	// Background Defaults
	$background_defaults = array(
		'color' => '#f9f9f9',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );
		
	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	
	
	// BEGIN OPTIONS ARRAY
	$options = array();

	// Basic Setings
	$options[] = array(
		'name' => __('Basic Settings', 'options_framework_theme'),
		'type' => 'heading');
		
		// Logo Image
		$options[] = array(
		'name' => __('Logo Image', 'options_framework_theme'),
		'desc' => __('Upload your custom logo (<strong>image must be no larger than 178x125</strong>)', 'options_framework_theme'),
		'id' => 'logo_image',
		'std' => $imagepath . 'logo.jpg',
		'type' => 'upload');
		
		// Main color Picker
		$options[] = array(
		'name' => __('Main Color', 'options_framework_theme'),
		'desc' => __('#0a95dd selected by default.', 'options_framework_theme'),
		'id' => 'main_color',
		'std' => '#0a95dd',
		'type' => 'color' );
		
		// Link color
		$options[] = array(
		'name' => __('Links Color', 'options_framework_theme'),
		'desc' => __('#0a95dd selected by default.', 'options_framework_theme'),
		'id' => 'link_color',
		'std' => '#0a95dd',
		'type' => 'color' );
		
		// Link hover color
		$options[] = array(
		'name' => __('Links Hover Color', 'options_framework_theme'),
		'desc' => __('#0a95dd selected by default.', 'options_framework_theme'),
		'id' => 'link_hover_color',
		'std' => '#333333',
		'type' => 'color' );
		
		// background image
		$options[] = array(
		'name' => __('Background Image', 'options_framework_theme'),
		'desc' => __('Upload your custom background image', 'options_framework_theme'),
		'id' => 'background_image',
		'std' => $background_defaults,
		'type' => 'background');
		
		// custom css
		$options[] = array(
		'name' => __('Custom CSS', 'options_framework_theme'),
		'desc' => __('You can add your own css here.', 'options_framework_theme'),
		'id' => 'custom_css',
		'type' => 'textarea');
		
		//blog page
		$options[] = array(
		'name' => __('Blog Page Title', 'options_framework_theme'),
		'desc' => __('Sets title for blog page, default (<strong>The Blog</strong>).', 'options_framework_theme'),
		'id' => 'blog_title',
		'std' => 'The Blog',
		'class' => 'mini',
		'type' => 'text');
		
		//404
		$options[] = array(
		'name' => __('404 Page Title', 'options_framework_theme'),
		'desc' => __('Sets title for 404 page, default (<strong>The Blog</strong>).', 'options_framework_theme'),
		'id' => 'fourohfour',
		'std' => 'WOOPS... SOMETHING\'S WRONG',
		'type' => 'text');
		
		// Case study
		$options[] = array(
		'name' => __('Single Portfolio Item Page Title', 'options_framework_theme'),
		'desc' => __('Sets title for single portfolio item page, default (<strong>The Blog</strong>).', 'options_framework_theme'),
		'id' => 'single_portfolio',
		'std' => 'Case Study',
		'type' => 'text');
		
		// Homepage Settings
	$options[] = array(
		'name' => __('Slider Settings', 'options_framework_theme'),
		'type' => 'heading');
		
		// show slogan
		$options[] = array(
		'name' => __('Show Home Slider', 'options_framework_theme'),
		'desc' => __('Show home slider (default shown).', 'options_framework_theme'),
		'id' => 'home_show_slider',
		'std' => '1',
		'type' => 'checkbox');
		
		$options[] = array(
		'name' => __('Select a Category', 'options_framework_theme'),
		'desc' => __('Select the category from which to display posts in the slider', 'options_framework_theme'),
		'id' => 'slider_select_categories',
		'type' => 'select',
		'options' => $options_categories);
		
		$options[] = array(
		'name' => __('Number of post items', 'options_framework_theme'),
		'desc' => __('How many post you want to display, set to 4 for default.', 'options_framework_theme'),
		'id' => 'slider_posts',
		'std' => '4',
		'class' => 'mini',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Show Controls', 'options_framework_theme'),
		'desc' => __('Show slider controls (default shown).', 'options_framework_theme'),
		'id' => 'home_show_slider_control',
		'std' => '1',
		'type' => 'checkbox');
		
		$options[] = array(
		'name' => __('Show Arrows', 'options_framework_theme'),
		'desc' => __('Show slider Prev/Next arrows (default shown).', 'options_framework_theme'),
		'id' => 'home_show_slider_arrows',
		'std' => '1',
		'type' => 'checkbox');
		
		
		
	// Homepage Settings
	$options[] = array(
		'name' => __('Home Settings', 'options_framework_theme'),
		'type' => 'heading');
		
		// show slogan
		$options[] = array(
		'name' => __('Home Slogan', 'options_framework_theme'),
		'desc' => __('Show home slogan (default shown).', 'options_framework_theme'),
		'id' => 'home_show_slogan',
		'std' => '1',
		'type' => 'checkbox');
		
		// text slogan
		$options[] = array(
		'name' => __('Home Slogan Text', 'options_framework_theme'),
		'desc' => __('Text to display in home as slogan.', 'options_framework_theme'),
		'id' => 'home_slogan_text',
		'std' => 'Lorem ipsum dolor sit amet, <a href="#">consectetur adipiscing</a> elit. Morbi imperdiet tortor id ligula vulputate volutpat.',
		'type' => 'textarea');
		
		//show slogan button
		$options[] = array(
		'name' => __('Home Slogan Button', 'options_framework_theme'),
		'desc' => __('Show home slogan button (default shown).', 'options_framework_theme'),
		'id' => 'home_show_slogan_button',
		'std' => '1',
		'type' => 'checkbox');
		
		// home slogan button text
		$options[] = array(
		'name' => __('Slogan button text', 'options_framework_theme'),
		'desc' => __('Text for button in slogan.', 'options_framework_theme'),
		'id' => 'home_show_slogan_button_text',
		'std' => 'Get Started',
		'class' => 'mini',
		'type' => 'text');
		
		// home slogan button link
		$options[] = array(
		'name' => __('Slogan button link', 'options_framework_theme'),
		'desc' => __('The url where you want this button to link.', 'options_framework_theme'),
		'id' => 'home_show_slogan_button_link',
		'std' => 'blog',
		'type' => 'text');
		
		// show services
		$options[] = array(
		'name' => __('Services', 'options_framework_theme'),
		'desc' => __('Show Services section (default shown).', 'options_framework_theme'),
		'id' => 'home_show_services',
		'std' => '1',
		'type' => 'checkbox');
		
		$options[] = array(
		'name' => __('Select a Page', 'options_framework_theme'),
		'desc' => __('page content to be pulled from this page.', 'options_framework_theme'),
		'id' => 'home_services_page',
		'type' => 'select',
		'options' => $options_pages);
		
		// show recent work
		$options[] = array(
		'name' => __('Recent work', 'options_framework_theme'),
		'desc' => __('Show Recent work (default shown).', 'options_framework_theme'),
		'id' => 'home_show_recent_work',
		'std' => '1',
		'type' => 'checkbox');
		
		$options[] = array(
		'name' => __('Recent work heading', 'options_framework_theme'),
		'desc' => __('Text for recent work heading.', 'options_framework_theme'),
		'id' => 'home_recent_work_text',
		'std' => 'Recent Work',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Number of post items', 'options_framework_theme'),
		'desc' => __('How many post you want to display, set to 6 for default.', 'options_framework_theme'),
		'id' => 'home_recent_work_posts',
		'std' => '6',
		'class' => 'mini',
		'type' => 'text');
		
	// Homepage about
	$options[] = array(
		'name' => __('Home About Settings', 'options_framework_theme'),
		'type' => 'heading');
		
		//show about us section
		$options[] = array(
		'name' => __('About Us', 'options_framework_theme'),
		'desc' => __('Show About Us section (default shown).', 'options_framework_theme'),
		'id' => 'home_show_about',
		'std' => '1',
		'type' => 'checkbox');
		
		// About Us heading
		$options[] = array(
		'name' => __('About Heading', 'options_framework_theme'),
		'desc' => __('Heading title for about section.', 'options_framework_theme'),
		'id' => 'home_about_heading',
		'std' => 'A Little About Us',
		'class' => 'mini',
		'type' => 'text');
		
		// testimonioals
		$options[] = array(
		'name' => __('Testimonials', 'options_framework_theme'),
		'desc' => __('Show testmonials, only appears if \'Abou Us\' is checked (default shown).', 'options_framework_theme'),
		'id' => 'home_show_testimonials',
		'std' => '1',
		'type' => 'checkbox');
		
	// Homepage about
	$options[] = array(
		'name' => __('Footer Social', 'options_framework_theme'),
		'type' => 'heading');
		
		$options[] = array(
		'name' => __('Twitter Feed', 'options_framework_theme'),
		'desc' => __('Display Twitter feed in footer (default shown).', 'options_framework_theme'),
		'id' => 'footer_twitter_feed',
		'std' => '1',
		'type' => 'checkbox');
		
		$options[] = array(
		'name' => __('Twitter', 'options_framework_theme'),
		'desc' => __('Your twitter username ex. ReneMerino .', 'options_framework_theme'),
		'id' => 'footer_twitter',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Facebook', 'options_framework_theme'),
		'desc' => __('Your facebook profile url.', 'options_framework_theme'),
		'id' => 'footer_facebook',
		'std' => 'http://',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Vimeo', 'options_framework_theme'),
		'desc' => __('Your Vimeo profile url.', 'options_framework_theme'),
		'id' => 'footer_vimeo',
		'std' => 'http://',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Youtube', 'options_framework_theme'),
		'desc' => __('Your YouTube profile url.', 'options_framework_theme'),
		'id' => 'footer_youtube',
		'std' => 'http://',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Skype', 'options_framework_theme'),
		'desc' => __('Your Skype username.', 'options_framework_theme'),
		'id' => 'footer_skype',
		'std' => '#',
		'type' => 'text');
			

	return $options;
} // end all options