 <?php
/*
 * Template Name: Testimonials
 *
 */
?>

<?php get_header() ?>

	
	<div class="clear"></div>
	
	<div id="pageintro">
	   <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; ?>
	</div>
	
	<hr />

<?php
    $args = array( 'category' => 29, 'post_type' =>  'post', 'numberposts' =>100 ); 
    $postslist = get_posts( $args );    
    foreach ($postslist as $post) :  setup_postdata($post); 
    ?>
    <div class="blogPost">
      <div class="post-thumbnail small-thm">
        <?php the_post_thumbnail(); ?>
      </div>
      


      
        <div class="excerpt"><em><?php the_content(); ?></em>
        <div class="clear"></div>
          <div class="title"><?php the_title(); ?></div>
        </div>
      
      </div>
       
   
    <?php endforeach; ?>


<?php get_footer() ?>

