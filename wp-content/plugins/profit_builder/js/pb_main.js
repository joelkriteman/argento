(function($){
    "use strict";
    $(document).ready(function(){
        if(typeof pbuilder != "undefined"){
            if(pbuilder.page_bg == "bgimage"){
                var theWindow        = $(window),
            	    $bg              = $("#bg"),
            	    aspectRatio      = $bg.width() / $bg.height();
            	                   			
            	theWindow.resize(function(){
                    if ((theWindow.width() / theWindow.height()) < aspectRatio)
            		    $bg.removeClass().addClass('bgheight');
            		else
            		    $bg.removeClass().addClass('bgwidth');
            	}).trigger("resize");
            }else if(pbuilder.page_bg == "videoembed"){
                if ( pbuilder.video_bg != 'none' ) {
            		var atts = {
            			mute : ( pbuilder.video_mute == 1 ? true : false ),
            			loop : ( pbuilder.video_loop == 1 ? true : false ),
            			hd : ( pbuilder.video_hd == 1 ? true : false )
            		};
            		pbuilderYoutube( "pb_page_bg_inner", pbuilder.video_bg, atts );
            	}
            }
        }
        
        $("[class*=timed-row-]").each(function(){
            var timed_row = this.className.match(/timed-row-(\d+)/i);
            var duration = timed_row ? parseInt(timed_row[1], 10) : 0;
            $(this).delay(duration).fadeIn();
        });
        
        $(window).on("resize", function(){
            $(".flipclock").each(function(){
                var $this = $(this);
                var col_width = $this.width();//console.log(col_width);

                if(col_width > 546){
                    $this.find(".flipclock_inner").css({'min-width':'460px'});
                }else{
                    var ul_width = col_width/12;
                    var ul_height = ul_width + 20;
                    var font_size = ul_height/2;
                    var dot_size = (ul_width - font_size - 2);
                    dot_size = dot_size < 6?6:dot_size;
                    
                    $this.find(".flipclock_inner").css({'min-width':'', "margin-left": "-"+(dot_size+2)+"px"});
                    $this.find(".flip-clock-wrapper ul").css({ "width": ul_width+"px", "height": ul_height+"px", "line-height": ul_height+"px", "margin": "5px 2px"});
                    $this.find(".flip-clock-wrapper ul li a div div.inn").css({ "font-size": font_size+"px", "width": "99%" });
                    $this.find(".flip-clock-wrapper ul li a div.up:after").css({ "top": (ul_height/2)+"px" });
                    $this.find(".flip-clock-divider").css({ "height": ul_height+"px", "width": (dot_size+2)+"px" });
                    $this.find(".flip-clock-dot").css({"height": dot_size+"px", "width": dot_size+"px", "left": ((dot_size+2)-dot_size-1)+"px"});
                    $this.find(".flip-clock-dot.top").css({ "top": (font_size-5)+"px" });
                    $this.find(".flip-clock-dot.bottom").css({ "bottom": (font_size - (dot_size*2))+"px" });
                    
                    $this.find(".flip-clock-divider .flip-clock-label").css({ "font-size": (font_size/2)+"px" });//console.log(ul_width+":"+font_size);
                    $this.find(".flip-clock-divider.days .flip-clock-label").css({"bottom": (font_size*-1)+"px", "left": (ul_width-(font_size/2)-(dot_size+2))+"px", "width": (ul_width*2)+"px"});
                    $this.find(".flip-clock-divider.hours .flip-clock-label").css({"bottom": (font_size*-1)+"px", "left": (ul_width-(font_size/2)-(dot_size+2))+"px", "width": (ul_width*2)+"px"});
                    $this.find(".flip-clock-divider.minutes .flip-clock-label").css({"bottom": (font_size*-1)+"px", "left": (ul_width-(font_size/2)-(dot_size+2))+"px", "width": (ul_width*2)+"px"});
                    $this.find(".flip-clock-divider.seconds .flip-clock-label").css({"bottom": (font_size*-1)+"px", "left": (ul_width-(font_size/2)-(dot_size+2))+"px", "width": (ul_width*2)+"px"});
                }
                
                
//                if(col_width > 546){
//                    $this.find(".flipclock_inner").css({'min-width':'460px'});
//                }else{
//                    $this.find(".flipclock_inner").css({'min-width':''});
//                    $this.find(".flip-clock-wrapper ul").css({ "height": "50px", "line-height": "50px", "margin": "5px 2px"});
//                    $this.find(".flip-clock-wrapper ul li a div.up:after").css({ "top": "24px" });
//                    $this.find(".flip-clock-divider").css({ "height": "50px" });
//                    $this.find(".flip-clock-dot").css({ "height": "6px", "width": "6px", "left": "7px"});
//                    $this.find(".flip-clock-dot.top").css({ "top": "17px" });
//                    $this.find(".flip-clock-dot.bottom").css({ "bottom": "8px" });
//	
//                    $this.find(".flip-clock-divider .flip-clock-label").css({ "font-size": "10px" });
//                    $this.find(".flip-clock-divider.days .flip-clock-label").css({"right": "-52px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.hours .flip-clock-label").css({"right": "-53px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.minutes .flip-clock-label").css({"right": "-60px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.seconds .flip-clock-label").css({"right": "-60px", "bottom": "-20px"});
//	
//                    $this.find(".flip-clock-wrapper ul").css({ "width": "37px" });
//                    $this.find(".flip-clock-wrapper ul li a div div.inn").css({ "font-size": "24px" });
//                }
            });
            
            $(".lightfluid").each(function(){
                var $this = $(this);
                var col_width = $this.width();//console.log(col_width);

                if(col_width > 546){
                    $this.find(".lightfluid_inner").css({'min-width':'460px', "width": ''});
                }else{
                    $this.find(".lightfluid_inner").css({'min-width':'', "width": col_width+'px'});
                }
                
                
//                if(col_width > 546){
//                    $this.find(".flipclock_inner").css({'min-width':'460px'});
//                }else{
//                    $this.find(".flipclock_inner").css({'min-width':''});
//                    $this.find(".flip-clock-wrapper ul").css({ "height": "50px", "line-height": "50px", "margin": "5px 2px"});
//                    $this.find(".flip-clock-wrapper ul li a div.up:after").css({ "top": "24px" });
//                    $this.find(".flip-clock-divider").css({ "height": "50px" });
//                    $this.find(".flip-clock-dot").css({ "height": "6px", "width": "6px", "left": "7px"});
//                    $this.find(".flip-clock-dot.top").css({ "top": "17px" });
//                    $this.find(".flip-clock-dot.bottom").css({ "bottom": "8px" });
//	
//                    $this.find(".flip-clock-divider .flip-clock-label").css({ "font-size": "10px" });
//                    $this.find(".flip-clock-divider.days .flip-clock-label").css({"right": "-52px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.hours .flip-clock-label").css({"right": "-53px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.minutes .flip-clock-label").css({"right": "-60px", "bottom": "-20px"});
//                    $this.find(".flip-clock-divider.seconds .flip-clock-label").css({"right": "-60px", "bottom": "-20px"});
//	
//                    $this.find(".flip-clock-wrapper ul").css({ "width": "37px" });
//                    $this.find(".flip-clock-wrapper ul li a div div.inn").css({ "font-size": "24px" });
//                }
            });
            
            $(".optin").each(function(){
                var $this = $(this);
                var col_width = $this.width();//console.log(col_width);
                if(col_width > 727){
                    var fields_count = $this.find(".field").length+1;
                    var fieldswidth = col_width/fields_count;
                    
                    if(col_width <= 642 && $this.hasClass("optin_style_Vertical")){
                        fieldswidth = col_width;
                    }else{
                        if(col_width > 548){
                            if(fieldswidth < 300)
                                fieldswidth = col_width;
                            else
                                fieldswidth -= 5;
                        }else{
                            fieldswidth -= 10;
                        }
                    }
                    
                        
                    //console.log(fields_count);
                    //console.log(fieldswidth);
                    
                    var padleft = parseInt($(this).find("input").eq(0).attr("padding-left"));
                    var padright = parseInt($(this).find("input").eq(0).attr("padding-right"));
                    
                    $this.find(".field").each(function(){
                        var fieldwidth = fieldswidth - 5;
                            
                        $(this).css({
                            'float':'left', 
                            'display':'block', 
                            "margin":"0 5px 10px 0",
                            'width':fieldwidth+'px', 
                            "padding-left":"0px", 
                            "padding-right":"0px",
                        });

                        var fieldwidth = fieldswidth - 5;
                        if(typeof $this.find('.field input').eq(0).css("box-sizing") == "undefined" || $this.find('.field input').eq(0).css("box-sizing")!= 'border-box')
                            fieldwidth = fieldswidth - padleft - padright - 5;
                                                              
                        $(this).find("input").css({
                            'float':'none', 
                            'display':'block',
                            "margin":"none",
                            "width":fieldwidth+"px", 
                            "padding-left":padleft+"px", 
                            "padding-right":padright+"px"
                        });
                    });
                    var fieldwidth = fieldswidth + padleft + padright;
                    var padleft = $this.find(".frb_button").css("padding-left");
                    var padright = $this.find(".frb_button").css("padding-right");
                        
                    padleft = (typeof padleft != "undefined" && padleft != null)?padleft.replace("px", ""):0;
                    padright = (typeof padright != "undefined" && padright != null)?padright.replace("px", ""):0;
                        
                    fieldwidth = fieldswidth - 5;
                    if(typeof $this.find('.field input').eq(0).css("box-sizing") == "undefined" || $this.find('.field input').eq(0).css("box-sizing")!= 'border-box')
                        fieldwidth = fieldswidth - padleft - padright - 5;
                        
                    $this.find(".frb_button").css({
                        'float':'left', 
                        'display':'block', 
                        "margin":"0 5px 10px 0",
                        'width':fieldwidth+'px',
                        'box-sizing': 'border-box',
                    })
                    .prev("div.clear").remove().before("<div class='clear' style='clear:both;'></div>")
                    .next("div.clear").remove().before("<div class='clear' style='clear:both;'></div>");
                    
                    var btntop = $this.find(".frb_button").position().top;
                    $this.find(".field").each(function(){
                        var thistop = $(this).position().top;
                        if(btntop == thistop){
                            var padtop = $this.find(".frb_button").css("padding-top");
                            var padbottom = $this.find(".frb_button").css("padding-bottom");
                            
                            padtop = (typeof padtop != "undefined" && padtop != null)?padtop.replace("px", ""):0;
                            padbottom = (typeof padbottom != "undefined" && padbottom != null)?padbottom.replace("px", ""):0;
                            
                            padtop = parseInt(padtop)+5;
                            padbottom = parseInt(padbottom)+5;
                            
                            $(this).find("input").css({ 
                                "padding-top":padtop+"px", 
                                "padding-bottom":padbottom+"px"
                            });                        
                        }else{
                            $(this).find("input").css({ 
                                "padding-top":"10px", 
                                "padding-bottom":"10px"
                            });
                        }
                        //console.log(thistop);
                    });
                }else{
                    var padleft = parseInt($(this).find(".field input").eq(0).attr("padding-left"));
                    var padright = parseInt($(this).find(".field input").eq(0).attr("padding-right"));
                    
                    $this.find(".field").each(function(){
                        var fieldwidth = col_width;    
                        $(this).css({
                            'float':'none', 
                            'display':'block', 
                            "margin":"none",
                            'width':fieldwidth+'px', 
                            "padding-left":"0px", 
                            "padding-right":"0px",
                        });
                        var fieldwidth = col_width;// - padleft - padright;
                        if(typeof $this.find('.field input').eq(0).css("box-sizing") == "undefined" || $this.find('.field input').eq(0).css("box-sizing")!= 'border-box')
                            fieldwidth = col_width - padleft - padright;

                        $(this).find("input").css({
                            'float':'none', 
                            'display':'block',
                            "margin":"none",
                            "width":fieldwidth+"px", 
                            "padding-left":padleft+"px", 
                            "padding-right":padright+"px"
                        });
                    });
                        
                    var fieldwidth = col_width + padleft + padright;
                    var padleft = $this.find(".frb_button").css("padding-left");
                    var padright = $this.find(".frb_button").css("padding-right");
                        
                    padleft = (typeof padleft != "undefined" && padleft != null)?padleft.replace("px", ""):0;
                    padright = (typeof padright != "undefined" && padright != null)?padright.replace("px", ""):0;
                    //console.log(col_width + ' ' + padleft + ' ' + padright);
                    
                    fieldwidth = col_width;// - padleft - padright;
                    $this.find(".frb_button").css({
                        'float':'none', 
                        'display':'block', 
                        'margin':'0px', 
                        'width':fieldwidth+'px',
                        'box-sizing': 'border-box',
                    }).prev("div.clear").remove().before("<div class='clear' style='clear:both;'></div>");
                }
            });
        }).trigger("resize");
        
        //removecufon();
    });
})(jQuery);

function setcookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getcookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function removecookie(name) {
    setcookie(name,"",-1);
}

function removecufon(){
    jQuery('cufon cufontext').each(function() {
        jQuery(this).closest('cufon').parent().append(jQuery(this).html());
        jQuery(this).closest('cufon').remove();
    });
}